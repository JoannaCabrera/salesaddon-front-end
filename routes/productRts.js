const express = require('express');
const router = express.Router();
const productsController = require('../controllers/productsCtrl');


// /* View Specific Material from the Database */
router.get('/view/specific/:materialId', (req, res) => {
	productsController.getSpecificProduct(req.params).then(result => res.send(result));
})

router.get('/view/allProducts', (req, res) => {
	productsController.getAllProducts(req.body).then( result => res.send(result))
})

module.exports = router;