const express = require('express');
const router = express.Router();
const auth = require('../auth')
const UserController = require('../controllers/user');

/* Register User */
router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
});
/* Log In User */
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
});
/*Authenticating User */
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
    UserController.getUserDetails(userData).then(user => res.send(user))
})
/*Change Password*/ 
router.put('/changePassword', auth.verify, (req, res) => {
    UserController.passwordchange(req.body).then(result => res.send(result))
})
module.exports = router;
