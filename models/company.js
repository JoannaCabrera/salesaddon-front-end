const mongoose = require('mongoose')

const companySchema = new mongoose.Schema({
    companyId: {
        type: String,
        require: true
    },
    companyName: {
        type: String,
        require: true
    },
    address: {
        type: String,
        require: true
    }

})

module.exports = mongoose.model('Company', companySchema);