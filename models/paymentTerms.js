const mongoose = require('mongoose')

const paymentTermsSchema = new mongoose.Schema({
    paymentTermsId: {
        type: String
    },
    paymentTermsDescription: {
        type: String
    }
})

module.exports = mongoose.model('PaymentTerms', paymentTermsSchema);