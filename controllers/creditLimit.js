let request = require("request")
const parseString = require('xml2js').parseString;

module.exports.creditLimit = (params) => {

 let accountNameId = params.accountNameId;

let xml = `<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global">
<soapenv:Header/>
<soapenv:Body>
    <glob:AccountsOpenAmountsQueryRequest_sync>
            <AccountOpenAmountsSelection>
                <SelectionByAccountID>
               <!--Optional:-->
               <InclusionExclusionCode>I</InclusionExclusionCode>
               <IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>
               <!--Optional:-->
               <LowerBoundaryIdentifier>${accountNameId}</LowerBoundaryIdentifier>
            </SelectionByAccountID>
            </AccountOpenAmountsSelection>
            <ProcessingConditions>
                <QueryHitsMaximumNumberValue>100</QueryHitsMaximumNumberValue>
                <QueryHitsUnlimitedIndicator>false</QueryHitsUnlimitedIndicator>
                <LastReturnedObjectID></LastReturnedObjectID>
            </ProcessingConditions>
</glob:AccountsOpenAmountsQueryRequest_sync>
</soapenv:Body>
</soapenv:Envelope>`

  request({
        method: 'POST',
        uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/queryaccountopenamountsin',
        auth: {
            'user': '_AOS0001',
            'pass': '@dd0nP@$$',
            'sendImmediately': false
        },
        headers: {
            'Content-Type': 'text/xml'
          },
        body: xml,
    }, 
     function (error, response, body) {
       if (error) {
          res.send ("Credit Limit Web Service Error")
       } else {
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for the Google homepage.
        parseString(body, function(err, result){
              let x = Object.values(result['soap-env:Envelope']['soap-env:Body']['0']['n0:AccountOpenAmountsQueryResponse_sync']['0']['AccountOpenAmounts']['0']['CreditLimitAmount']['0'])
              //  res.send(x) 
              //  return response.body
              console.log(x)
              return x
       })

		}
  })
}