const SalesOrder = require('../models/SalesOrderDb');
const request = require('request');
const req = require('express/lib/request');
const parseString = require('xml2js').parseString

/*Add Sales Order*/
module.exports.addSalesOrder = (reqBody) => {

	let newSalesOrder = new SalesOrder({
        salesOrderNo: reqBody.salesOrderNo,
        address: reqBody.address,
        accountId: reqBody.accountId,
        accountName: reqBody.accountName,
        shipToPartyId: reqBody.shipToPartyId,
        shipToPartyDescription: reqBody.shipToPartyDescription,
        paymentTermsId: reqBody.paymentTermsId,
        paymentTerms: reqBody.paymentTerms,
        requestedDate: reqBody.requestedDate,
        externalReference: reqBody.externalReference,
        comments: reqBody.comments,
        customerInformation: reqBody.customerInformation,
        docStatus: reqBody.docStatus,
        creationDate: reqBody.creationDate,
        requestor: reqBody.requestor,
        employeeId: reqBody.employeeId,
        currency: reqBody.currency,
        sapSoId: '',
        salesGroup: reqBody.salesGroup,
        mbAccountId: reqBody.mbAccountId,
        underTolerance: reqBody.underTolerance,
        overTolerance: reqBody.overTolerance,
        deliveryDate: reqBody.deliveryDate,
        tps: reqBody.tps,
        deliveryBlockReason: reqBody.deliveryBlockReason,
        reason: '',
        items: reqBody.items
	})

	return newSalesOrder.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the sales order in the database!`;
		} else {
			return `Successfully added the sales order in the database!`;
		}
	})
};

/*Retrieve All Sales Order in the Database*/
module.exports.getAll = () => {
    return SalesOrder.find().then( result => {
        return result
    })
}


/*Retrieve All Sales Order All In-Prep Status */
module.exports.getAllInPrepStatus = (reqParams) => {
    return SalesOrder.find({docStatus: 'In Preparation'}).then( result => {
        return result
    })
}

/*Retrieve All Sales Order All Cancelled Status */
module.exports.getAllCancelledStatus = (reqParams) => {
    return SalesOrder.find({docStatus: 'Cancelled'}).then( result => {
        return result
    })
}

/*Retrieve All Sales Order in the Database Customer Specific*/
module.exports.getAllSalesOrder = (reqParams) => {
    return SalesOrder.find({employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve All Sales Order All Status (approverView)*/
module.exports.getAllStatusApprover = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved'}).then( result => {
        return result
    })
}


/*Retrieve All Sales Order With ForApproval Status (approverView)*/
module.exports.getAllForApproval = (reqParams) => {
    return SalesOrder.find({docStatus: 'For Approval'}).then( result => {
        return result
    })
}

/*Retrieve All Sales Order With Rejected Status (approverView)*/
module.exports.getAllRejected = (reqParams) => {
    return SalesOrder.find({docStatus: 'Rejected'}).then( result => {
        return result
    })
}
/*Retrieve All Sales Order With Approved Status (approverView)*/
module.exports.getAllApproved = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved'}).then( result => {
        return result
    })
}

/*Retrieve In Preparation Sales Order*/
module.exports.inPreparationSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'In Preparation', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve For Approval Sales Order*/
module.exports.forApprovalSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'For Approval', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Approved Sales Order*/
module.exports.approvedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Approved', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Posted Sales Order*/
module.exports.postedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Posted', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Rejected Sales Order*/
module.exports.rejectedSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Rejected', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}

/*Retrieve Specific Sales Order in the Database using Document ID*/
module.exports.getSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        return result
    })
} 

/*Edit the Retrieved Specific Sales Order to update in the Database*/
module.exports.editSalesOrder = (reqBody, reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then( result => {
        result.address = reqBody.address
        result.salesOrderNo = reqBody.salesOrderNo
        result.accountId = reqBody.accountId
        result.accountName = reqBody.accountName
        result.shipToPartyId = reqBody.shipToPartyId
        result.shipToPartyDescription = reqBody.shipToPartyDescription
        result.paymentTermsId = reqBody.paymentTermsId
        result.paymentTerms = reqBody.paymentTerms
        result.requestedDate = reqBody.requestedDate
        result.externalReference = reqBody.externalReference
        result.comments = reqBody.comments
        result.customerInformation = reqBody.customerInformation
        result.docStatus = reqBody.docStatus
        result.creationDate = reqBody.creationDate
        result.currency = reqBody.currency
        result.requestor = reqBody.requestor
        result.employeeId = reqBody.employeeId
        result.salesGroup = reqBody.salesGroup
        result.mbAccountId = reqBody.mbAccountId
        result.underTolerance = reqBody.underTolerance
        result.overTolerance = reqBody.overTolerance
        result.deliveryDate = reqBody.deliveryDate
        result.tps = reqBody.tps
        result.sapSoId = reqBody.sapSoId
        result.items = reqBody.items

        return result.save().then((user, error) => {
            if(error){
                return `Error: Failed to update the sales order in the database!`;
            } else {
                return `Successfully updated the sales order in the database!`;
            }
        })
    })
}

        
/*Edit the Status of the Retrieved Specific Sales Order to For Approval*/
module.exports.submitForApproval = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'For Approval'

        return result.save().then((user, error) => {
            if(error) {
                return `Error: Failed to update the sales order status in the database!`
            } else {
                return `Successfully updated the sales order status in the database!`
            }
        })
    }
)}

// Delete Function
module.exports.deleteSalesOrder = (reqParams) => {
    return SalesOrder.deleteOne({salesOrderNo: reqParams.salesOrderNo}).then((result, error) => {
        if(error) {
            return `Error: Failed to delete the sales order in the database!`
        } else {
            return `Successfully delete the sales order in the database!`
        }
    }
)}

/*Approve Function*/
module.exports.approveSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        let year = result.deliveryDate.slice(6,10)
        let month = result.deliveryDate.slice(0,2)
        let day = result.deliveryDate.slice(3,5)
        let updatedDateTime = `${year}-${month}-${day}T12:00:00.1234567Z`

        let xmlItems = ''
        for(let i = 0; i < result.items.length; i++) {
            xmlItems += `
                <Item>
                    <ID>${i+1}</ID>
                    <ItemProduct>
                        <ProductInternalID>${result.items[i].productId.replaceAll("-", "/")}</ProductInternalID>
                    </ItemProduct>

                    <ItemScheduleLine>
                        <Quantity>${result.items[i].quantity}</Quantity>
                    </ItemScheduleLine>

                    <a47:CustomerExternalCode>${result.items[i].partNumber}</a47:CustomerExternalCode>

                    <PriceAndTaxCalculationItem>
                        <CountryCode>PH</CountryCode>

                        <ItemMainDiscount>
                            <Rate>
                                <DecimalValue>${(result.items[i].discount !== '') ? result.items[i].discount : 0}</DecimalValue>
                                <CurrencyCode>PH</CurrencyCode>
                                <BaseDecimalValue>1</BaseDecimalValue>
                            </Rate>
                        </ItemMainDiscount>

                        <ItemMainPrice>
                            <Rate>
                                <DecimalValue>${(result.items[i].listPrice !== '') ? result.items[i].listPrice : 0}</DecimalValue>
                                <CurrencyCode>PHP</CurrencyCode>
                                <BaseDecimalValue>1</BaseDecimalValue>
                            </Rate>
                        </ItemMainPrice>

                    </PriceAndTaxCalculationItem>
                </Item>
            `
        }

        let xml = `<?xml version="1.0" encoding="utf-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:glob="http://sap.com/xi/SAPGlobal20/Global" xmlns:a47="http://sap.com/xi/AP/CustomerExtension/BYD/A475U">
            <soapenv:Header/>
            <soapenv:Body>
                <n0:SalesOrderBundleMaintainRequest_sync xmlns:n0="http://sap.com/xi/SAPGlobal20/Global">
                    <SalesOrder>
                    <BuyerID>${result.externalReference}</BuyerID>

                    <AccountParty>
                        <PartyID>${result.accountId}</PartyID>
                    </AccountParty>

                    <BillToParty>
                        <PartyID>${result.accountId}</PartyID>
                    </BillToParty>

                    <EmployeeResponsibleParty>
                        <PartyID>${result.employeeId}</PartyID>
                    </EmployeeResponsibleParty>

                    <SalesAndServiceBusinessArea actionCode="01">
                        <DistributionChannelCode>02</DistributionChannelCode>
                    </SalesAndServiceBusinessArea>

                    <RequestedFulfillmentPeriodPeriodTerms> 
                        <StartDateTime timeZoneCode="UTC">${updatedDateTime}</StartDateTime> 
                    </RequestedFulfillmentPeriodPeriodTerms>

                    <TextCollection> 
                        <Text>
                            <TypeCode>10011</TypeCode>
                            <ContentText>${result.comments}</ContentText> 
                        </Text> 
                    </TextCollection>

                    ${xmlItems}

                    </SalesOrder>
                </n0:SalesOrderBundleMaintainRequest_sync>
            </soapenv:Body>
        </soapenv:Envelope>`

        request({
            method: 'POST',
            uri: 'https://my355904.sapbydesign.com/sap/bc/srt/scs/sap/managesalesorderin5',
            auth: {
                'user': '_AOS0001',
                'pass': '@dd0nP@$$',
                'sendImmediately': false
            },
            headers: {
                'Content-Type': 'text/xml'
                },
            body: xml,
        }, function (error, response, body) {
            if(error) {
                return "Request Error!"
            } else {
                let soid = ''
                let errorLogs = ''
                parseString(body, (error, data) => {
                    if(data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0'].hasOwnProperty('SalesOrder')) {
                        soid = data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0']['SalesOrder']['0']['ID']['0']
                    } else {
                        errorLogs = data['soap-env:Envelope']['soap-env:Body']['0']['n0:SalesOrderBundleMaintainConfirmation_sync']['0']['Log']['0']['Item']    
                    }
                })

                if(soid.length > 0) {
                    result.sapSoId = soid
                    result.docStatus = 'Posted'
                }

                if(errorLogs.length > 0) {
                    result.sapPrId = ''
                    result.docStatus = 'Posting Error'
                    result.reason = errorLogs
                }
                
                let month = new Date().toLocaleString('default', { month: 'long' })

                if(month === 'January'){
                    month = '01'
                } else if(month === 'February') {
                    month = '02'
                } else if(month === 'March') {
                    month = '03'
                } else if(month === 'April') {
                    month = '04'
                } else if(month === 'May') {
                    month = '05'
                } else if(month === 'June') {
                    month = '06'
                } else if(month === 'July') {
                    month = '07'
                } else if(month === 'August') {
                    month = '08'
                } else if(month === 'September') {
                    month = '09'
                } else if(month === 'October') {
                    month = '10'
                } else if(month === 'November') {
                    month = '11'
                } else if(month === 'December') {
                    month = '12'
                }

                let day = new Date().getDate();
                let year = new Date().getFullYear()
                let time = new Date().toLocaleTimeString()

                if(time.length === 10) {
                    time = `0${time}`
                } else {
                    time = time
                }
                
                result.approvedDate = `${month}/${day}/${year} - ${time}`

                result.save().then((updatedResult, error) => {
                    if(error) {
                        return false
                    } else {
                        return true
                    }
                })
            }
        })
    }
)}

/*Reject Function*/
module.exports.rejectSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Rejected'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed Reject the Sales Order!`
            } else {
                return `Successfully Rejected the Sales Order!`
            }
        })
    }
)}

/*Post Function*/
module.exports.postSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Posted'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed to Post the Sales Order!`
            } else {
                return `Successfully Posted the Sales Order!`
            }
        })
    }
)}
/*submitForApproval Function*/
module.exports.submitForApprovalSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'For Approval'

        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed to Post the Sales Order!`
            } else {
                return `Successfully Submitted For Approval!`
            }
        })
    }
)}


/*Retrieve Cancelled Sales Order*/
module.exports.cancelledSalesOrder = (reqParams) => {
    return SalesOrder.find({docStatus: 'Cancelled', employeeId: reqParams.employeeId}).then( result => {
        return result
    })
}
    
/*Cancel Function*/
module.exports.cancelSalesOrder = (reqParams) => {
    return SalesOrder.findOne({salesOrderNo: reqParams.salesOrderNo}).then(result => {
        result.docStatus = 'Cancelled'
    
        return result.save().then((result, error) => {
            if(error) {
                return `Error: Failed to Cancel the Sales Order!`
            } else {
                return `Successfully Cancelled the Sales Order!`
            }
        })
    })
}

/*Retrieve All Status Sales Order  (approver)*/
module.exports.getAllStatusApprover = (reqParams) => {
    return SalesOrder.find({}).then( result => {
        let resultArray = []
        result.forEach(data => {
            if(data.docStatus === 'For Approval') {
                resultArray.push(data)
            } else if(data.docStatus === 'Approved') {
                resultArray.push(data)
            } else if(data.docStatus === 'Returned') {
                resultArray.push(data)
            } else if(data.docStatus === 'Rejected') {
                resultArray.push(data)
            }
        })

        return resultArray
    })
}

module.exports.getAllSpecificCustomer = async (reqParams) => {
    return await SalesOrder.find({accountId: reqParams.accountId}).then(result =>{
        let selectedArray = []
        for(let i = 0; i < result.length; i++) {
            if(result[i].docStatus === 'In Preparation') {
                selectedArray.push(result[i])
            } else if(result[i].docStatus === 'For Approval') {
                selectedArray.push(result[i])
            }
        }
        let allItemsArray = []
        for(let x = 0; x < selectedArray.length; x++) {
            for(let y = 0; y < selectedArray[x].items.length; y++) {
                allItemsArray.push(selectedArray[x].items[y])
            }
        }

        return allItemsArray
    })
}

