const PaymentTerms = require('../models/paymentTerms');

// Add a Product to the database
module.exports.addTerms = (reqBody) => {
	let newPaymentTerms = new PaymentTerms ({
		paymentTermsId: reqBody.paymentTermsId,
		paymentTermsDescription : reqBody.paymentTermsDescription,
	})


	return newPaymentTerms.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the payment term in the database!`;
		} else {
			return `Successfully added the payment term in the database!`;
		}
	})

}

// View All Products from the database
module.exports.viewAllTerms = () => {
	return PaymentTerms.find().then(result => {
		return result;
	})
}

// View Specific Payment Term by ID from the database
module.exports.viewTerm = (reqParams) => {
	return PaymentTerms.find({paymentTermsId: reqParams.paymentTermsId}).then(result => {
		if(Object.keys(result).length < 1) {
			return `No payment term with an ID of ${reqParams.paymentTermsId}`;
		} else {
			return result
		}
	})
}