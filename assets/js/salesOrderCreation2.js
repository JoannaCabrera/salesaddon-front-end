// Global Variables
let token = localStorage.getItem("token")
let salesOrderNoContainer = document.getElementById('salesOrderId')
let accountNameContainer = document.getElementById('accountName')
let accountNameId = ""
let shipToContainer = document.getElementById("shipTo")
let shipToId = ''
let paymentTermsContainer = document.getElementById("paymentTerms");
let termsId = ''
let creationDateContainer = document.getElementById('createdOn');
let currencyContainer = document.getElementById("currency");
let employeeNameContainer = document.getElementById('requestor')
let employeeIdContainer = document.getElementById('employeeId')

let productContainer = document.getElementById('product');
let productDescriptionContainer = document.getElementById("productDescription");
let quantityContainer = document.getElementById('quantity')
let uomContainer = document.getElementById("uom");
let listPriceContainer = document.getElementById("listPrice");
let discountContainer = document.getElementById('discount')
let netPriceContainer = document.getElementById('netPrice');
let taxAmountContainer = document.getElementById('taxAmount')
let netValueContainer = document.getElementById('netValue')
let taxIdentifier = '';

// Logout function
function logoutFunction(){
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.querySelector(".userIcon").onclick = function() {
	logoutFunction()
}

// Buttons
let salesOrderMenu = document.getElementById("buttonRow")

if(localStorage.getItem("isRequestor") === "true") {
	salesOrderMenu.innerHTML =
	`
	<button id="saveAndViewButton">Save and View</button>
	<button id="saveAndNewButton">Save and New</button>
    <button id="postButton">Post</button>
	<button id="forApprovalButton">Submit For Approval</button>
	<button id="closeButton">Close</button>
	`

	document.getElementById('saveAndViewButton').onclick = function(e){
		e.preventDefault()
		generateCreationDate()
		generateSoIdSaveAndView()
	}

	document.getElementById('saveAndNewButton').onclick = function(e){
		e.preventDefault()
		generateCreationDate()
		generateSoIdSaveAndNew()
	}
    document.getElementById('postButton').onclick = function(e){
		e.preventDefault()
		generateCreationDate()
		postFunction()
	}

	document.getElementById('forApprovalButton').onclick = function(e){
		e.preventDefault()
		generateCreationDate()
		generateSoIdForApproval()
	}

	document.getElementById("closeButton").onclick = function(){
		closeFunction()
	}
}

// Generate Sales Order ID on Save and View
function generateSoIdSaveAndView() {
	let salesOrderId = document.getElementById("salesOrderId");
	fetch(`https://tranquil-dawn-06180.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		salesOrderId.value = data.length + 1
	}).then(() => {
		saveAndViewFunction()
	})
}

// Save And View Function
function saveAndViewFunction() {
	let accountNameValue = accountNameContainer.value
	let shipToValue = shipToContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let requestedDateValue = document.getElementById('requestedDate').value
	let externalReferenceValue = document.getElementById('externalReference').value
	let commentsValue = document.getElementById('comment').value
	let creationDateValue = creationDateContainer.value
	let currencyValue = currencyContainer.value
	let requestorValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
	let salesOrderNoValue = salesOrderNoContainer.value

	if(accountNameValue === '') {
		alert('Please indicate the Account Name')
	} else if(itemsArray.length === 0) {
		alert('Please add items to Sales Order')
	} else {
		fetch('https://tranquil-dawn-06180.herokuapp.com/api/salesOrder/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				salesOrderNo: salesOrderNoValue,
				accountId: accountNameId,
				accountName: accountNameValue,
				shipToPartyId: shipToId,
				shipToPartyDescription: shipToValue,
				paymentTermsId: termsId,
				paymentTerms: paymentTermsValue,
				requestedDate: requestedDateValue,
				externalReference: externalReferenceValue,
				comments: commentsValue,
				docStatus: 'In Preparation',
				creationDate: creationDateValue,
				currency: currencyValue,
				requestor: requestorValue,
				employeeId: employeeIdValue,
				items: itemsArray
			})
		}).then(() => {
			alert('Successfully Created the Sales Order ')
			window.location.replace(`./salesOrderViewing.html?documentId=${salesOrderNoValue}`)
		})
	}
}

// Generate Sales Order ID on Save and New
function generateSoIdSaveAndNew() {
	let salesOrderId = document.getElementById("salesOrderId");
	fetch(`https://tranquil-dawn-06180.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		salesOrderId.value = data.length + 1
	}).then(() => {
		saveAndNewFunction()
	})
}

// Save and New Function
function saveAndNewFunction() {
	let accountNameValue = accountNameContainer.value
	let shipToValue = shipToContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let requestedDateValue = document.getElementById('requestedDate').value
	let externalReferenceValue = document.getElementById('externalReference').value
	let commentsValue = document.getElementById('comment').value
	let creationDateValue = creationDateContainer.value
	let currencyValue = currencyContainer.value
	let requestorValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
	let salesOrderNoValue = salesOrderNoContainer.value

	if(accountNameValue === '') {
		alert('Please indicate the Account Name')
	} else if(itemsArray.length === 0) {
		alert('Please add items to Sales Order')
	} else {
		fetch('https://tranquil-dawn-06180.herokuapp.com/api/salesOrder/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				salesOrderNo: salesOrderNoValue,
				accountId: accountNameId,
				accountName: accountNameValue,
				shipToPartyId: shipToId,
				shipToPartyDescription: shipToValue,
				paymentTermsId: termsId,
				paymentTerms: paymentTermsValue,
				requestedDate: requestedDateValue,
				externalReference: externalReferenceValue,
				comments: commentsValue,
				docStatus: 'In Preparation',
				creationDate: creationDateValue,
				currency: currencyValue,
				requestor: requestorValue,
				employeeId: employeeIdValue,
				items: itemsArray
			})
		}).then(res => {
			alert('Successfully Created the Sales Order')
			window.location.reload()
		})
	}
}

// Generate Sales Order ID on Submit For Approval
function generateSoIdForApproval() {
	let salesOrderId = document.getElementById("salesOrderId");
	fetch(`https://tranquil-dawn-06180.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		salesOrderId.value = data.length + 1
	}).then(() => {
		submitForApprovalFunction()
	})
}

function submitForApprovalFunction() {
	let accountNameValue = accountNameContainer.value
	let shipToValue = shipToContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let requestedDateValue = document.getElementById('requestedDate').value
	let externalReferenceValue = document.getElementById('externalReference').value
	let commentsValue = document.getElementById('comment').value
	let creationDateValue = creationDateContainer.value
	let currencyValue = currencyContainer.value
	let requestorValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
	let salesOrderNoValue = salesOrderNoContainer.value

	if(accountNameValue === '') {
		alert('Please indicate the Account Name')
	} else if(itemsArray.length === 0) {
		alert('Please add items to Sales Order')
	} else {
		fetch('https://tranquil-dawn-06180.herokuapp.com/api/salesOrder/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				salesOrderNo: salesOrderNoValue,
				accountId: accountNameId,
				accountName: accountNameValue,
				shipToPartyId: shipToId,
				shipToPartyDescription: shipToValue,
				paymentTermsId: termsId,
				paymentTerms: paymentTermsValue,
				requestedDate: requestedDateValue,
				externalReference: externalReferenceValue,
				comments: commentsValue,
				docStatus: 'For Approval',
				creationDate: creationDateValue,
				currency: currencyValue,
				requestor: requestorValue,
				employeeId: employeeIdValue,
				items: itemsArray
			})
		}).then(res => {
			alert('Successfully Submitted the Sales Order for Approval')
			window.location.reload()
		})
	}
}
// Post Function Status Marker
function postFunction() {
	let accountNameValue = accountNameContainer.value
	let shipToValue = shipToContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let requestedDateValue = document.getElementById('requestedDate').value
	let externalReferenceValue = document.getElementById('externalReference').value
	let commentsValue = document.getElementById('comment').value
	let creationDateValue = creationDateContainer.value
	let currencyValue = currencyContainer.value
	let requestorValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
	let salesOrderNoValue = salesOrderNoContainer.value

	if(accountNameValue === '') {
		alert('Please indicate the Account Name')
	} else if(itemsArray.length === 0) {
		alert('Please add items to Sales Order')
	} else {
		fetch('https://tranquil-dawn-06180.herokuapp.com/api/salesOrder/salesOrderMonitoring/post', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				salesOrderNo: salesOrderNoValue,
				accountId: accountNameId,
				accountName: accountNameValue,
				shipToPartyId: shipToId,
				shipToPartyDescription: shipToValue,
				paymentTermsId: termsId,
				paymentTerms: paymentTermsValue,
				requestedDate: requestedDateValue,
				externalReference: externalReferenceValue,
				comments: commentsValue,
				docStatus: 'Posted',
				creationDate: creationDateValue,
				currency: currencyValue,
				requestor: requestorValue,
				employeeId: employeeIdValue,
				items: itemsArray
			})
		}).then(res => {
			alert('Successfully Submitted the Sales Order/s for Posting')
			window.location.reload()
		})
	}
}


// Close Function
function closeFunction(){
	let confirm = window.confirm ("Do you want to close? All unsaved data will be deleted");
	if (confirm == true){
		 window.location.replace('./salesOrderMonitoring.html')
	}		
}

//Look Up - Account Name
let accountsDatalist = document.getElementById("accounts")
let account;

function fetchCompanies() {
	fetch('https://tranquil-dawn-06180.herokuapp.com/api/customer/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		account = data.map(result => {
			return  (
				account =
				`
				<option value="${result.accountName}">
				${result.accountId}
				</option>
				`
			)
		})
		accountsDatalist.innerHTML = account;
	}) 
}

fetchCompanies();



//Populate Fields - Ship To Location, Payment Terms, Currency, Tax Identifier
async function populateCustomerFields() {

    fetch(`https://tranquil-dawn-06180.herokuapp.com/api/customer/view/specific/${accountNameContainer.value}`, {
   
           method: 'GET',
   
           headers: {
   
               'Content-Type': 'application/json'
   
           }
   
       }).then(res => res.json()).then(data => {
   
           accountNameId = data.accountId
   
           shipToId = data.shipToPartyId
   
           shipToContainer.value = data.shipToPartyDescription
   
           termsId = data.paymentTermsId
   
           paymentTermsContainer.value = data.paymentTerms
   
           currencyContainer.value = data.currency
   
           taxIdentifier = data.taxStatus
   
           let credit = document.getElementById('creditLimitContainer');
   
           let url =  'https://tranquil-dawn-06180.herokuapp.com/'
   
           let query = data.accountId
   
           let address = url + query
   
   await =
   
       fetch(address, {
   
       }).then( res => res.text())
   
       .then( data => {
   
       let trim = data.replace('",{"currencyCode":"PHP"}]', '')
   
       let trim2 = trim.replace('["', '')
   
       credit.value =  trim2
   
       })
      
   
       })
   
   }

// Populate Fields - Employee Name and Employee ID
function populateEmployeeFields() {
	fetch('https://tranquil-dawn-06180.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${token}`
	}
	}).then(res => res.json()).then(result => {
		employeeNameContainer.value = `${result.firstName} ${result.lastName}`;
		employeeIdContainer.value = result.employeeId;
	})
}

//Look Up - Currency
function currencyLookUp() {
	let currencyDatalist = document.getElementById('currencies')
	let curr;
	fetch('https://tranquil-dawn-06180.herokuapp.com/api/currency/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		curr = data.map(result => {
			return  (
				curr =
				`
				<option value="${result.currencyTicker.toUpperCase()}">
				${result.currencyName}
				</option>
				`
			)
		})
		currencyDatalist.innerHTML = curr;
	}) 
}

// Look Up - Payment Terms
function paymentTermsLookUp() {
	let termsDatalist = document.getElementById('terms')
	let terms;

	fetch('https://tranquil-dawn-06180.herokuapp.com/api/terms/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		// console.log(data);
		terms = data.map(result => {
			return  (
				`
				<option value="${result.paymentTermsDescription}">
				${result.paymentTermsId}
				</option>
				`
			)
		})
		termsDatalist.innerHTML = terms;
	}) 
}

function shipToLocationCustomerLookUp() {
	let locationDatalist = document.getElementById('locations')
	let location;

	fetch('https://tranquil-dawn-06180.herokuapp.com/api/shipToLocationCustomer/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		// console.log(data);
		location = data.map(result => {
			return  (
				`
				<option value="${result.shipToPartyDescription}">
				${result.shipToPartyId}
				</option>
				`
			)
		})
		locationDatalist.innerHTML = location;
	}) 
}

accountNameContainer.oninput = function() {
	populateCustomerFields()
	populateEmployeeFields()
	currencyLookUp()
	paymentTermsLookUp()
	shipToLocationCustomerLookUp()
};

//Generating Created On Date
function generateCreationDate() {
	let month = new Date().toLocaleString('default', { month: 'long' })

	if(month === 'January'){
		month = '01'
	} else if(month === 'February') {
		month = '02'
	} else if(month === 'March') {
		month = '03'
	} else if(month === 'April') {
		month = '04'
	} else if(month === 'May') {
		month = '05'
	} else if(month === 'June') {
		month = '06'
	} else if(month === 'July') {
		month = '07'
	} else if(month === 'August') {
		month = '08'
	} else if(month === 'September') {
		month = '09'
	} else if(month === 'October') {
		month = '10'
	} else if(month === 'November') {
		month = '11'
	} else if(month === 'December') {
		month = '12'
	}

	let day = new Date().getDate();
	let year = new Date().getFullYear();

	creationDateContainer.value = `${month}/${day}/${year}`
}

//Look Up - Products
let productDatalist = document.getElementById('products')
let product;
fetch('https://tranquil-dawn-06180.herokuapp.com/api/products/view/allProducts', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	// console.log(data);
	product = data.map(result => {
		return  (
			`
			<option value="${result.materialId}">
			${result.materialDescription}
			</option>
			`
		)
	})
	productDatalist.innerHTML = product;
})

//Populate Fields - Product Description , Unit of Measure, List Price, and Net Price
function populateProductFields() {
	fetch(`https://tranquil-dawn-06180.herokuapp.com/api/products/${productContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		productDescriptionContainer.value = data.materialDescription
		listPriceContainer.value = data.price
		uomContainer.value = data.uomDescription
		netPriceContainer.value= data.price
	})
}

function enableFields() {
	uomContainer.removeAttribute('readonly')
	quantityContainer.removeAttribute('readonly')
	discountContainer.removeAttribute('readonly')
}

//Look Up - Unit of Measure
function uomLookUp() {
	let uomDatalist = document.getElementById('uoms')
	let uom;
	fetch('https://tranquil-dawn-06180.herokuapp.com/api/uom/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		uom = data.map(result => {
			return  (
				uom =
				`
				<option value="${result.description}">
				${result.code}
				</option>
				`
			)
		})
		uomDatalist.innerHTML = uom;
	})
}

productContainer.oninput = function() {
	populateProductFields()
	enableFields()
	uomLookUp()
};

// Generating Net Value
function generateNetValue() {
	let quantityValue = document.getElementById('quantity').value
	let netValueContainer = document.getElementById('netValue')
	let listPriceValue = document.getElementById('listPrice').value

	netValueContainer.value = quantityValue * listPriceValue
};

quantityContainer.oninput = function() {
	generateNetValue()
};

// Changing the Net Price based on Discount Field
function applyDiscount(discountValue) {
	let netPriceValue = document.getElementById("netPrice").value
	let listPriceValue = document.getElementById("listPrice").value
	netPriceValue = listPriceValue;
	discountValue = discountValue/100
	netPriceValue = netPriceValue * (1 - discountValue)

	changeNetPrice(netPriceValue)
}

function changeNetPrice(netPriceValue) {
	let netPriceContainer = document.getElementById("netPrice")
	let netValueContainer = document.getElementById("netValue")
	let quantityValue = document.getElementById("quantity").value

	netPriceContainer.value = netPriceValue
	netValueContainer.value = quantityValue * netPriceContainer.value

	calculateTaxAmount();
}

function calculateTaxAmount() {
	let netValueValue = document.getElementById("netValue").value
	let taxAmountContainer = document.getElementById("taxAmount")

	if(taxIdentifier === 'Vatable') {
		taxAmountContainer.value = netValueValue * 0.12
	} else {
		taxAmountContainer.value = netValueValue * 0
	}

	itemsPush();
	itemsDisplay();
	clearFunction();
}

// document.getElementById("discount").onchange = function() {
// 	let discountValue = document.getElementById("discount").value
// 	if(discountValue !== '') {
// 		applyDiscout(discountValue)
// 	} else {
// 		let listPriceValue = document.getElementById('listPrice').value
// 		netPriceContainer.value = listPriceValue
// 	}
// }

//Array for Products/Items
let itemsArray = [];

function itemsPush() {	
	itemsArray.push({
		productId: productContainer.value,
		productDescription: productDescriptionContainer.value,
		quantity: quantityContainer.value,
		unitOfMeasure: uomContainer.value,
		listPrice: listPriceContainer.value,
		discount: discountContainer.value,
		netPrice: netPriceContainer.value,
		taxAmount: taxAmountContainer.value,
		netValue: netValueContainer.value,
	})
}

// Add Row
let productTableContainer = document.getElementById("productTableContainer");
let items;

function itemsDisplay(){
	let a = 0;
	items = itemsArray.map(itemsList=> {
        return(
			a++,
            `
            <tr>
				<td>${a}</td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productId}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productDescription}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.quantity}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.unitOfMeasure}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.listPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.discount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.taxAmount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netValue}" disabled></td>
				<td>
				<div class="d-flex justify-content-center">
					<button class="crudButton button btn ms-1" type="button" onclick="remove_tr(this)">
						<i class="far fa-trash-alt"></i>
					</button>
				</div>
			</td>
            </tr>
            `
        )
	}).join("");
	productTableContainer.innerHTML = items; 
}

// To Clear Look Ups
function clearFunction() {
	productContainer.value = ''
	productDescriptionContainer.value = ''
	quantityContainer.value = ''
	uomContainer.value = ''
	listPriceContainer.value = ''
	discountContainer.value = ''
	netPriceContainer.value = ''
	taxAmountContainer.value = ''
	netValueContainer.value = ''
}

function disableFields() {
	uomContainer.setAttribute('readonly', true)
	quantityContainer.setAttribute('readonly', true)
	discountContainer.setAttribute('readonly', true)
}

document.getElementById("addItems").onclick = function(e) {
	e.preventDefault();
	let discountValue = discountContainer.value
	applyDiscount(discountValue);
	disableFields();
};

// Remove Functions
function remove_tr(This) {
	itemsArray.splice(This.closest('tr').rowIndex - 1, 1);
	This.closest('tr').remove();
}
