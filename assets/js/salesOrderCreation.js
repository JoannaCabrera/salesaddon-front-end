// Global Variables
let token = localStorage.getItem("token");
let accountNameId = '';
let shipToId = '';
let termsId = '';
let taxIdentifier = '';
let employeePosition = '';


let salesOrderNoContainer = document.getElementById('salesOrderId');
let accountNameContainer = document.getElementById('accountName');
let shipToContainer = document.getElementById("shipTo");
let paymentTermsContainer = document.getElementById("paymentTerms");
let creationDateContainer = document.getElementById('createdOn');
let currencyContainer = document.getElementById("currency");
let employeeNameContainer = document.getElementById('requestor');
let employeeIdContainer = document.getElementById('employeeId');
let salesGroupContainer = document.getElementById('salesGroup');
let mbAccountIdContainer = document.getElementById('mbAccountId');
let underToleranceContainer = document.getElementById('underTolerance');
let overToleranceContainer = document.getElementById('overTolerance');
let orderDeliveryDate = document.getElementById('deliveryDate');
let externalReferenceContainer = document.getElementById('externalReference')
let addressContainer = document.getElementById('address')
let deliveryBlockContainer = document.getElementById('deliveryBlock')
let isExternalReferenceUnique = false;

let productContainer = document.getElementById('product');
let productDescriptionContainer = document.getElementById("productDescription");
let leadTimeContainer = document.getElementById('leadTime');
let quantityContainer = document.getElementById('quantity');
let uomContainer = document.getElementById("uom");
let listPriceContainer = document.getElementById("listPrice");
let discountContainer = document.getElementById('discount')
let netPriceContainer = document.getElementById('netPrice');
let taxAmountContainer = document.getElementById('taxAmount');
let netValueContainer = document.getElementById('netValue');
let moqContainer = document.getElementById('moq');
let stockContainer = document.getElementById('stocks')
let partNumberContainer = document.getElementById('partNumber')
let itemsArray = [];
let leadTime = '';
let productCategory = ''

let totalNetValueContainer = document.getElementById('totalNetValue')
let totalTaxAmountContainer = document.getElementById('totalTaxAmount')
let itemstotalAmountContainer = document.getElementById('itemstotalAmount')


// Logout function
function logoutFunction(){
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.querySelector(".userIcon").onclick = function() {
	logoutFunction()
}

// Buttons
let salesOrderMenu = document.getElementById("buttonRow")

if(localStorage.getItem("isRequestor") === "true") {
	salesOrderMenu.innerHTML =
	`
	<button id="saveAndViewButton">Save and View</button>
	<button id="saveAndNewButton">Save and New</button>
	<button id="closeButton">Close</button>

	`

	document.getElementById('saveAndViewButton').onclick = function(e) {
		e.preventDefault()
		generateSoIdSaveAndView()
	}

	document.getElementById('saveAndNewButton').onclick = function(e) {
		e.preventDefault()
		generateSoIdSaveAndNew()
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault()
		closeFunction()
	}
}

// Generate Sales Quotes ID on Save and View
function generateSoIdSaveAndView() {
	let accountNameValue = accountNameContainer.value
	let deliveryBlockValue = document.getElementById ("deliveryBlock").value
	let creditLimitValue = document.getElementById ("creditLimit").value
	let currentOrderValue = document.getElementById ("itemstotalAmount").value

	if(accountNameValue === '') {
		alert('Please indicate the Account Name')
	} else if(itemsArray.length === 0) {
		alert('Please add items to Sales Quotes')
	} else if(currentOrderValue > creditLimitValue && deliveryBlockValue === ''){
		alert ("Error! Credit limit exceeded. Please select a delivery block reason")		
	} else {
		fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(results => {
			if(results.length > 0) {
				for(let i = 0; i < results.length; i++) {
					if(results[i].externalReference === externalReferenceContainer.value) {
						isExternalReferenceUnique = false
						externalReferenceContainer.value = ''
						alert('External Reference is already used')
						break;
					} else {
						isExternalReferenceUnique = true
					}
				}
			} else {
				isExternalReferenceUnique = true
			}
		}).then(() => {
			if(isExternalReferenceUnique === true) {
				let salesOrderId = document.getElementById("salesOrderId");
				fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(res => res.json()).then(data => {
					salesOrderId.value = data.length + 1
				}).then(() => {
					generateCreationDate()
				}).then(() => {				
					saveAndViewFunction()				
			
				})
			}
		})
	}
}

// Save And View Function
function saveAndViewFunction() {
	let accountNameValue = accountNameContainer.value
	let shipToValue = shipToContainer.value
	let addressValue = addressContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let requestedDateValue = document.getElementById('requestedDate').value
	let externalReferenceValue = externalReferenceContainer.value
	let commentsValue = document.getElementById('comment').value
	let custInformationValue = document.getElementById('custInformation').value
	let creationDateValue = creationDateContainer.value
	let currencyValue = currencyContainer.value
	let requestorValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
	let salesOrderNoValue = salesOrderNoContainer.value
	let salesGroupValue = salesGroupContainer.value
	let mbAccountIdValue = mbAccountIdContainer.value
	let underToleranceValue = underToleranceContainer.value
	let overToleranceValue = overToleranceContainer.value
	let orderDeliveryDateValue = orderDeliveryDate.value
	let tpsValue = document.getElementById('tps').value
	
	fetch('https://fierce-crag-19923.herokuapp.com/api/salesOrder/add', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			address: addressValue,
			salesOrderNo: salesOrderNoValue,
			accountId: accountNameId,
			accountName: accountNameValue,
			shipToPartyId: shipToId,
			shipToPartyDescription: shipToValue,
			paymentTermsId: termsId,
			paymentTerms: paymentTermsValue,
			requestedDate: requestedDateValue,
			externalReference: externalReferenceValue,
			comments: commentsValue,
			customerInformation: custInformationValue,
			docStatus: 'In Preparation',
			creationDate: creationDateValue,
			currency: currencyValue,
			requestor: requestorValue,
			employeeId: employeeIdValue,
			salesGroup: salesGroupValue,
			mbAccountId: mbAccountIdValue,
			underTolerance: underToleranceValue,
			overTolerance: overToleranceValue,
			deliveryDate: orderDeliveryDateValue,
			tps: tpsValue,
			deliveryBlockReason: deliveryBlockContainer.value,
			items: itemsArray
		})
	}).then(() => {
		alert('Successfully Created the Sales Quotes ')
		window.location.replace(`./salesOrderViewing.html?documentId=${salesOrderNoValue}`)
	})
}

// Generate Sales Quotes ID on Save and New
function generateSoIdSaveAndNew() {
	let accountNameValue = accountNameContainer.value

	if(accountNameValue === '') {
		alert('Please indicate the Account Name')
	} else if(itemsArray.length === 0) {
		alert('Please add items to Sales Quotes')
	} else {
		fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(results => {
			if(results.length > 0) {
				for(let i = 0; i < results.length; i++) {
					if(results[i].externalReference === externalReferenceContainer.value) {
						isExternalReferenceUnique = false
						externalReferenceContainer.value = ''
						alert('External Reference is already used')
						break;
					} else {
						isExternalReferenceUnique = true
					}
				}
			} else {
				isExternalReferenceUnique = true
			}
		}).then(() => {
			if(isExternalReferenceUnique === true) {
				let salesOrderId = document.getElementById("salesOrderId");
				fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(res => res.json()).then(data => {
					salesOrderId.value = data.length + 1
				}).then(() => {
					generateCreationDate()				
					saveAndNewFunction()				
				})
			}
		})
	}
}

// Save and New Function
function saveAndNewFunction() {
	let accountNameValue = accountNameContainer.value
	let shipToValue = shipToContainer.value
	let addressValue = addressContainer.value
	let paymentTermsValue = paymentTermsContainer.value
	let requestedDateValue = document.getElementById('requestedDate').value
	let externalReferenceValue = externalReferenceContainer.value
	let commentsValue = document.getElementById('comment').value
	let custInformationValue = document.getElementById('custInformation').value
	let creationDateValue = creationDateContainer.value
	let currencyValue = currencyContainer.value
	let requestorValue = employeeNameContainer.value
	let employeeIdValue = employeeIdContainer.value
	let salesOrderNoValue = salesOrderNoContainer.value
	let salesGroupValue = salesGroupContainer.value
	let mbAccountIdValue = mbAccountIdContainer.value
	let underToleranceValue = underToleranceContainer.value
	let overToleranceValue = overToleranceContainer.value
	let orderDeliveryDateValue = orderDeliveryDate.value
	let tpsValue = document.getElementById('tps').value
	let deliveryBlockValue = document.getElementById ("deliveryBlock").value

	
	let creditLimit = document.getElementById ("creditLimit").value
	let currentOrder = document.getElementById ("itemstotalAmount").value

	if(currentOrder > creditLimit){
		let confirm = window.confirm ("Credit limit exceeded; please select delivery block reason");
		
	}fetch('https://fierce-crag-19923.herokuapp.com/api/salesOrder/add', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			address: addressValue,
			salesOrderNo: salesOrderNoValue,
			accountId: accountNameId,
			accountName: accountNameValue,
			shipToPartyId: shipToId,
			shipToPartyDescription: shipToValue,
			paymentTermsId: termsId,
			paymentTerms: paymentTermsValue,
			requestedDate: requestedDateValue,
			externalReference: externalReferenceValue, 
			comments: commentsValue,
			customerInformation: custInformationValue,
			docStatus: 'In Preparation',
			creationDate: creationDateValue,
			currency: currencyValue,
			requestor: requestorValue,
			employeeId: employeeIdValue,
			salesGroup: salesGroupValue,
			mbAccountId: mbAccountIdValue,
			underTolerance: underToleranceValue,
			overTolerance: overToleranceValue,
			deliveryDate: orderDeliveryDateValue,
			tps: tpsValue,
			deliveryBlockReason: deliveryBlockValue,
			items: itemsArray
		})
	}).then(res => {
		alert('Successfully Created the Sales Quotes')
		window.location.reload()
	})
}

// Close Function
function closeFunction(){
	let confirm = window.confirm ("Do you want to close? All unsaved data will be deleted");
	if (confirm === true){
		 window.location.replace('./salesOrderMonitoring.html')
	}		
}

//Look Up - Account Name
let accountsDatalist = document.getElementById("accounts")
let account;

function fetchCompanies() {
	fetch('https://fierce-crag-19923.herokuapp.com/api/customer/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		account = data.map(result => {
			return  (
				account =
				`
				<option value="${result.accountName}">
				${result.accountId} - ${result.searchName}
				</option>
				`
			)
		})
		accountsDatalist.innerHTML = account;
	}) 
}

fetchCompanies();


//Check CSL over In-prep and for-Approval

function updateCSL(){
	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase/${accountNameId}`,{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {

		let totalValueForSpecificCustomer = 0

        for(a = 0; a < result.length; a++) {
            let totalAmountPerItem = 0
            totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
            totalValueForSpecificCustomer += totalAmountPerItem
        }

		let creditLimit = document.getElementById ("creditLimit")
		let csl = localStorage.getItem("csl");
		console.log(csl)
		console.log(creditLimit.value)
		console.log(totalValueForSpecificCustomer)
       
		creditLimit.value = (csl - totalValueForSpecificCustomer).toFixed(2)	

	})
}

//Delivery block function
// let updatedcreditLimit = document.getElementById ("creditLimit").value

// function enableDeliveryBlockField(){
	
// 	if (updatedcreditLimit < 0){
// 		console.log(updatedcreditLimit)
// 		document.getElementById("deliveryBlock").setAttribute("disabled", true);
		 	
// 	}else{
// 		document.getElementById("deliveryBlock").setAttribute("disabled", false);
// }
// }


//Populate Fields - Ship To Location, Payment Terms, Currency, Tax Identifier, Address
async function populateCustomerFields() {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/customer/view/specific/${accountNameContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		addressContainer.value = data.address
		salesGroupContainer.value = data.salesGroup
		mbAccountIdContainer.value = data.mbAccountId
		underToleranceContainer.value = data.underTolerance
		overToleranceContainer.value = data.overTolerance
		accountNameId = data.accountId
		shipToId = data.shipToPartyId
		shipToContainer.value = data.shipToPartyDescription
		termsId = data.paymentTermsId
		paymentTermsContainer.value = data.paymentTerms
		currencyContainer.value = data.currency
		taxIdentifier = data.taxStatus
	}).then(() => {
		fetchBydCsl()
	})
}

function fetchBydCsl() {
	let credit = document.getElementById('creditLimit');
	let url =  'https://vast-shelf-95326.herokuapp.com/getLimit/'
	let query = accountNameId
	let address = url + query

	fetch(address, {
		method: 'GET'
	}).then(res => res.text()).then(data => {
		let trim = data.replace('",{"currencyCode":"PHP"}]', '')
		let csl = trim.replace('["', '')
		credit.value = csl.value
		localStorage.setItem("csl",csl);
	}).then(() => {
		updateCSL()
	})
}

// Populate Fields - Employee Name and Employee ID
function populateEmployeeFields() {
	fetch('https://fierce-crag-19923.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		Authorization: `Bearer ${token}`
	}
	}).then(res => res.json()).then(result => {
		employeePosition = result.position
		employeeNameContainer.value = `${result.firstName} ${result.lastName}`;
		employeeIdContainer.value = result.employeeId;

		determineEmployeePosition()
	})
}


// Determine Employee Position for Lead Time Field
function determineEmployeePosition() {
	if(employeePosition === 'Customer Service Supervisor') {
		listPriceContainer.removeAttribute('readonly')
	}
}

//Look Up - Currency
function currencyLookUp() {
	let currencyDatalist = document.getElementById('currencies')
	let curr;
	fetch('https://fierce-crag-19923.herokuapp.com/api/currency/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		curr = data.map(result => {
			return  (
				curr =
				`
				<option value="${result.currencyTicker.toUpperCase()}">
				${result.currencyName}
				</option>
				`
			)
		})
		currencyDatalist.innerHTML = curr;
	}) 
}

// Look Up - Payment Terms
function paymentTermsLookUp() {
	let termsDatalist = document.getElementById('terms')
	let terms;

	fetch('https://fierce-crag-19923.herokuapp.com/api/terms/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		terms = data.map(result => {
			return  (
				`
				<option value="${result.paymentTermsDescription}">
				${result.paymentTermsId}
				</option>
				`
			)
		})
		termsDatalist.innerHTML = terms;
	}) 
}

// Look Up - Ship To Location Customer
function shipToLocationCustomerLookUp() {
	let locationDatalist = document.getElementById('locations')
	let location;

	fetch('https://fierce-crag-19923.herokuapp.com/api/shipToLocationCustomer/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		location = data.map(result => {
			return  (
				`
				<option value="${result.shipToPartyDescription}">
				${result.shipToPartyId}
				</option>
				`
			)
		})
		locationDatalist.innerHTML = location;
	}) 
}

accountNameContainer.oninput = function() {
	populateCustomerFields()
	populateEmployeeFields()
	currencyLookUp()
	paymentTermsLookUp()
	shipToLocationCustomerLookUp()
	productContainer.removeAttribute('readonly')
	document.getElementById('addItems').removeAttribute('disabled')
};

//Generating Created On Date
function generateCreationDate() {
	let month = new Date().toLocaleString('default', { month: 'long' })

	if(month === 'January'){
		month = '01'
	} else if(month === 'February') {
		month = '02'
	} else if(month === 'March') {
		month = '03'
	} else if(month === 'April') {
		month = '04'
	} else if(month === 'May') {
		month = '05'
	} else if(month === 'June') {
		month = '06'
	} else if(month === 'July') {
		month = '07'
	} else if(month === 'August') {
		month = '08'
	} else if(month === 'September') {
		month = '09'
	} else if(month === 'October') {
		month = '10'
	} else if(month === 'November') {
		month = '11'
	} else if(month === 'December') {
		month = '12'
	}

	let day = new Date().getDate();
	let year = new Date().getFullYear();

	creationDateContainer.value = `${month}/${day}/${year}`
}

// Generate Order Delivery Date
function generateOrderDeliveryDate() {
	let deliveryDate = document.getElementById('deliveryDate')
	let today = new Date()
	let newDate = new Date()
	newDate.setDate(today.getDate() + parseInt(leadTime))

	newMonth = newDate.toLocaleString('default', {month: 'long'})

	if(newMonth === 'January'){
		newMonth = '01'
	} else if(newMonth === 'February') {
		newMonth = '02'
	} else if(newMonth === 'March') {
		newMonth = '03'
	} else if(newMonth === 'April') {
		newMonth = '04'
	} else if(newMonth === 'May') {
		newMonth = '05'
	} else if(newMonth === 'June') {
		newMonth = '06'
	} else if(newMonth === 'July') {
		newMonth = '07'
	} else if(newMonth === 'August') {
		newMonth = '08'
	} else if(newMonth === 'September') {
		newMonth = '09'
	} else if(newMonth === 'October') {
		newMonth = '10'
	} else if(newMonth === 'November') {
		newMonth = '11'
	} else if(newMonth === 'December') {
		newMonth = '12'
	}

	newDay = newDate.getDate()
	
	if(newDay.toString().length === 1) {
		newDay = `0${newDay}`
	}
	
	newYear = newDate.getFullYear()
	
	deliveryDate.value = `${newMonth}/${newDay}/${newYear}`
}

// Look Up - TPS
let tpsList = document.getElementById("tps")
let tps;

fetch('https://fierce-crag-19923.herokuapp.com/api/tps/view/all', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	tps = data.map(result => {
		return  (
			`
			<option value="${result.code}">${result.code} - ${result.vendorAccount}</option>
			`
		)
	})
	tpsList.innerHTML = tps;      
})

//Look Up - Products
let productDatalist = document.getElementById('products')
let product;
fetch('https://fierce-crag-19923.herokuapp.com/api/products/view/allProducts', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	product = data.map(result => {
		return  (
			`
			<option value="${result.materialId}">
			${result.materialDescription}
			</option>
			`
		)
	})
	productDatalist.innerHTML = product;
})

//Populate Fields - Product Description , Unit of Measure, List Price, Net Price, Lead Time, Stocks
function populateProductFields() {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/products/view/specific/${productContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		productDescriptionContainer.value = data.materialDescription
		listPriceContainer.value = data.price
		uomContainer.value = data.uomDescription
		netPriceContainer.value = data.price
		leadTimeContainer.value = data.leadTime
		stockContainer.value = data.stock
		partNumberContainer.value = data.partNumber
		productCategory = data.productCategoryId

		if(leadTime === '') {
			leadTime = data.leadTime
		} else if(leadTime < parseInt(data.leadTime)) {
			leadTime = data.leadTime
		}
	}).then(() => {
		validateMoq()
	})
}

function enableFields() {
	uomContainer.removeAttribute('readonly')
	quantityContainer.removeAttribute('readonly')
	discountContainer.removeAttribute('readonly')
	partNumberContainer.removeAttribute('readonly')
	listPriceContainer.removeAttribute('readonly')
}

//Look Up - Unit of Measure
function uomLookUp() {
	let uomDatalist = document.getElementById('uoms')
	let uom;
	fetch('https://fierce-crag-19923.herokuapp.com/api/uom/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		uom = data.map(result => {
			return  (
				uom =
				`
				<option value="${result.description}">
				${result.code}
				</option>
				`
			)
		})
		uomDatalist.innerHTML = uom;
	})
}

// Look-Up and Validation of Minimum Order Quantity
function validateMoq() {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/moq/validate/${productCategory}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {
		if(result === true) {
			// 8754M-050B-00BLK
			fetch(`https://fierce-crag-19923.herokuapp.com/api/moq/view/specific/${productCategory}`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(res => res.json()).then(data => {
				moqContainer.value = data.minimumOrderQuantity
			})
		}
	})
}

productContainer.oninput = function() {
	populateProductFields()
	enableFields()
	uomLookUp()
};

// Generating Net Value
function generateNetValue() {
	let quantityValue = document.getElementById('quantity').value
	let netValueContainer = document.getElementById('netValue')
	let listPriceValue = document.getElementById('listPrice').value

	netValueContainer.value = quantityValue * listPriceValue
};

quantityContainer.oninput = function() {
	generateNetValue()
};

// Changing the Net Price based on Discount Field
function applyDiscount(discountValue) {
	let netPriceValue = document.getElementById("netPrice").value
	let listPriceValue = document.getElementById("listPrice").value
	netPriceValue = listPriceValue;
	discountValue = discountValue/100
	netPriceValue = (netPriceValue * (1 - discountValue)).toFixed(2)

	changeNetPrice(netPriceValue)
}

function changeNetPrice(netPriceValue) {
	let netPriceContainer = document.getElementById("netPrice")
	let netValueContainer = document.getElementById("netValue")
	let quantityValue = document.getElementById("quantity").value

	netPriceContainer.value = netPriceValue
	netValueContainer.value = (quantityValue * netPriceContainer.value).toFixed(2)

	calculateTaxAmount();
}

function calculateTaxAmount() {
	let netValueValue = document.getElementById("netValue").value
	let taxAmountContainer = document.getElementById("taxAmount")

	if(taxIdentifier === 'Vatable') {
		taxAmountContainer.value = (netValueValue * 0.12).toFixed(2)
	} else {
		taxAmountContainer.value = netValueValue * 0
	}
	
	itemsPush();
	itemsDisplay();
	clearFunction();
}


//Array for Products/Items
function itemsPush() {	
	itemsArray.push({
		productId: productContainer.value,
		productDescription: productDescriptionContainer.value,
		leadTime: leadTimeContainer.value,
		partNumber: partNumberContainer.value,
		quantity: quantityContainer.value,
		stocks: stockContainer.value,
		moq: moqContainer.value,
		unitOfMeasure: uomContainer.value,
		listPrice: listPriceContainer.value,
		discount: discountContainer.value,
		netPrice: netPriceContainer.value,
		taxAmount: taxAmountContainer.value,
		netValue: netValueContainer.value,
	})
}

// Add Row
let productTableContainer = document.getElementById("productTableContainer");
let items;

function itemsDisplay(){
	let a = 0;
	items = itemsArray.map(itemsList=> {
        return(
			a++,
            `
            <tr>
				<td>${a}</td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productId}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productDescription}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.leadTime}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.partNumber}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.quantity}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.stocks}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.moq}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.unitOfMeasure}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.listPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.discount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.taxAmount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netValue}" disabled></td>
				<td>
				<div class="d-flex justify-content-center">
					<button class="crudButton button btn ms-1" type="button" onclick="remove_tr(this)">
						<i class="far fa-trash-alt"></i>
					</button>
				</div>
				
				</td>
		
            </tr>
			
            `
        )
	}).join("");
	productTableContainer.innerHTML = items; 
}

// To Clear Look Ups
function clearFunction() {
	productContainer.value = ''
	productDescriptionContainer.value = ''
	leadTimeContainer.value = ''
	partNumberContainer.value = ''
	quantityContainer.value = ''
	stockContainer.value = ''
	moqContainer.value = ''
	uomContainer.value = ''
	listPriceContainer.value = ''
	discountContainer.value = ''
	netPriceContainer.value = ''
	taxAmountContainer.value = ''
	netValueContainer.value = ''
}

function disableFields() {
	uomContainer.setAttribute('readonly', true)
	quantityContainer.setAttribute('readonly', true)
	discountContainer.setAttribute('readonly', true)
	listPriceContainer.setAttribute('readonly', true)
	partNumberContainer.setAttribute('readonly', true)
}

document.getElementById("addItems").onclick = function(e) {
	e.preventDefault();
	if(parseFloat(quantityContainer.value) <= parseFloat(stockContainer.value)) {
		if(parseFloat(quantityContainer.value) < parseFloat(moqContainer.value)) {
			let confirm = window.confirm (`Ordered Quantity is less than Minimum Order Quantity of ${moqContainer.value}. Do you want to proceed?`);
			if (confirm === true){
				let discountValue = discountContainer.value
				let orderedQuantity = quantityContainer.value
				applyDiscount(discountValue);
				disableFields();
				generateOrderDeliveryDate();
				updateTotalNetValue();
				updateTotalTaxAmount();
				calculateTotalAmount();
				updateStockCount(orderedQuantity);
			}
		} else {
			let discountValue = discountContainer.value
			let orderedQuantity = quantityContainer.value
			applyDiscount(discountValue);
			disableFields();
			generateOrderDeliveryDate();
			updateTotalNetValue();
			updateTotalTaxAmount();
			calculateTotalAmount();
			updateStockCount(orderedQuantity);
		}
	} else {
		alert('Stock count for the product is not enough.')
	}	
};

// Remove Functions
function remove_tr(This) {
	itemsArray.splice(This.closest('tr').rowIndex - 1, 1);
	This.closest('tr').remove();

	updateTotalNetValue();
	updateTotalTaxAmount();
	calculateTotalAmount();
}

function updateStockCount(orderedQuantity) {
	// console.log(orderedQuantity)
}

function updateTotalNetValue() {
	let sumOfAllNetValue = 0
	itemsArray.forEach(item => {
		sumOfAllNetValue += parseFloat(item.netValue)
	})
	totalNetValueContainer.value = sumOfAllNetValue.toFixed(2)
}

function updateTotalTaxAmount() {
	let sumOfAllTaxAmount = 0
	itemsArray.forEach(item => {
		sumOfAllTaxAmount += parseFloat(item.taxAmount)
	})
	totalTaxAmountContainer.value = sumOfAllTaxAmount.toFixed(2)
}

function calculateTotalAmount() {
	let sumOfAllItemsTotalAmount = 0
	itemsArray.forEach(item => {
		sumOfAllItemsTotalAmount += parseFloat(item.taxAmount) + parseFloat(item.netValue)
	})
	itemstotalAmountContainer.value = sumOfAllItemsTotalAmount.toFixed(2)

	let creditLimitValue = document.getElementById("creditLimit").value


	if ( itemstotalAmountContainer.value > creditLimitValue ) {
		document.getElementById("deliveryBlock").disabled = false;
	} else {
		document.getElementById("deliveryBlock").disabled = true;
	}
}

