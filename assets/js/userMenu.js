// Global Variables
let employeeId =localStorage.getItem("employeeId")
let token = localStorage.getItem("token");
let firstName = localStorage.getItem('firstName')
let lastName = localStorage.getItem('lastName')

// Welcome Banner
// let userLoggedIn = document.getElementById('userLoggedIn')
// userLoggedIn.innerHTML = `${firstName} ${lastName}`

// Logout Function
function logoutFunction() {
  localStorage.clear();
  window.location.replace('./logout.html')
}

document.getElementById("logoutButton").onclick = function() {
	logoutFunction();
}

// Sales Order Menu
let SalesOrderMenu = document.getElementById("SalesOrderMenu")

if(localStorage.getItem("isApprover") === "true") {
  
	SalesOrderMenu.innerHTML = 
	`
  <div class="btnContainer">
    <Button class="iconBtn" onclick="salesOrderForApproval()">
      <div class="btnHeader">Sales Order For Approval</div>
        <div class="btn-flex">  
          <img src="./assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="approverCounter" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>
  `
  
  let approverCounter = document.getElementById("approverCounter");
  fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/status/allForApprovalStatus`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }).then(res => res.json()).then(data => {
    approverCounter.innerHTML = data.length
  })

} else {

	SalesOrderMenu.innerHTML =
	`
  <div class="btnContainer">
    <Button class="iconBtn" onclick="allSalesOrder()">
      <div class="btnHeader">All Sales Quotes</div>
        <div class="btn-flex">  
          <img src="./assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="counterOne" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>

  <div class="btnContainer">
    <Button class="iconBtn" onclick="salesOrderMonitoring()">
      <div class="btnHeader">My Sales Quotes</div>
        <div class="btn-flex">  
          <img src="./assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
        </div>
        <div id="counterTwo" style="margin-left: 2rem ; margin-top: 1rem">0</div>
      </div>         
    </Button>
  </div>
	`

  let counterOne = document.getElementById("counterOne");
  fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json()).then(data => {
    counterOne.innerHTML = data.length
  })

  let counterTwo = document.getElementById("counterTwo");
  fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/specificRequestor/${employeeId}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json()).then(data => {
    counterTwo.innerHTML = data.length
  })

}

function salesOrderMonitoring(){
  location.replace("./salesOrderMonitoring.html")
}

function salesOrderForApproval(){
  location.replace("./salesOrderMonitoringFA.html")
}

function allSalesOrder(){
  location.replace("./allSalesOrder.html")
}
