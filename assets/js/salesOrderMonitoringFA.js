// Global Variables
let token = localStorage.getItem("token") 
let employeeId =localStorage.getItem("employeeId")
let isApprover = localStorage.getItem("isApprover")

//Logout Function 
function logoutFunction() {
    localStorage.clear();
    window.location.replace('./logout.html')
}

document.getElementById('userIcon').onclick = function() {
    logoutFunction();
}

//Filter Status
let filterStatus = document.getElementById("filterStatus");

if (localStorage.getItem("isApprover") === "true"){
    filterStatus.innerHTML = 
    `
    <option value="forApprovalStatusApprover" id="forApprovalStatusApprover">For Approval</option>
    <option value="allStatusApprover" id="allStatusApprover">All Status</option>   
    <option value="rejectedStatusApprover" id="rejectedStatusApprover">Rejected</option>
    `
}

// Buttons
let buttonContainerList = document.getElementById("buttonRow")

if(localStorage.getItem("isApprover") === "true") {
	buttonContainerList.innerHTML = 
	`
	<button id="viewButtonApprover">View</button>
    <button id="approveButton">Approve</button>
    <button id="rejectButton">Reject</button>
	<button id="closeButton">Close</button>
	`

    document.getElementById('viewButtonApprover').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        viewFunctionApprover()
    }

    document.getElementById('approveButton').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        approveFunction()
    }

    document.getElementById('rejectButton').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        rejectFunction()
    }

    document.getElementById("closeButton").onclick = function(e) {
        e.preventDefault()
        closeFunction()
    }
}

//View Function Approver
function viewFunctionApprover() {
    if(selectedSalesOrders.length < 1) {
        alert('Please Select a Sales Order')
    }
    else if(selectedSalesOrders.length > 1) {
        selectedSalesOrders.length = 0
        alert('Action cannot be performed on multiple line items. Please select a single line item')
    } else {
        let docId = selectedSalesOrders[0].salesQuoteNo
        window.location.replace(`./salesOrderViewing.html?documentId=${docId}`)
    }
}

// Approve Function
function approveFunction() {
    if(selectedSalesOrders.length < 1) {
        alert('Please Select a Sales Order')
    }else {
        let wrongStatus = [];
        for(let i = 0; i < selectedSalesOrders.length; i++) {
            if(selectedSalesOrders[i].docStatus !== 'For Approval') {
                wrongStatus.push({
                    salesQuoteNo: selectedSalesOrders[i].salesQuoteNo,
                    docStatus: selectedSalesOrders[i].docStatus
                })
                continue;
            } else {
                fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/approve/${selectedSalesOrders[i].salesQuoteNo}`, {
                    method: 'POST'
                })
            }
        }

        if(wrongStatus.length < 1) {
            alert('Successfully Approved the Sales Order/s')	
        } else {
            alert(`Some Sales Order/s failed to Approve.`)
        }

        selectedSalesOrders.length = 0
    }
}

// Reject Function
function rejectFunction() {
    if(selectedSalesOrders.length < 1) {
        alert('Please Select a Sales Order')
    }else {
        let wrongStatus = [];
        for(let i = 0; i < selectedSalesOrders.length; i++) {
            if(selectedSalesOrders[i].docStatus !== 'For Approval') {
                wrongStatus.push({
                    salesQuoteNo: selectedSalesOrders[i].salesQuoteNo,
                    docStatus: selectedSalesOrders[i].docStatus
                })
                continue;
            } else {
                fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/reject/${selectedSalesOrders[i].salesQuoteNo}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
            }
        }

        if(wrongStatus.length < 1) {
            alert('Successfully Rejected the Sales Order/s')	
        } else {
            alert(`Some Sales Order/s failed to Reject.`)
        }
	
        selectedSalesOrders.length = 0
        window.location.reload()
    }
}

// Close Function
function closeFunction(){
	window.location.replace('./launchpad.html')
}


// Select All Checkbox
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}

let selectedSalesOrders = []

// Select a Checkbox
function getValuesOfCheckedBox() {
    //Reference the Table.
    let grid = document.getElementById("salesOrderTable");

    //Reference the CheckBoxes in Table.
    let checkBoxes = grid.getElementsByTagName("INPUT");

    //Loop through the CheckBoxes.
    for (let i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            let row = checkBoxes[i].parentNode.parentNode
            let data = {
                salesOrderId: row.cells[1].innerHTML,
                salesQuoteNo: row.cells[2].innerText,
                accountId: row.cells[3].innerHTML,
                accountName: row.cells[4].innerHTML,
                totalAmount: row.cells[5].innerHTML,
                externalReference: row.cells[6].innerHTML,
                docStatus: row.cells[7].innerHTML,
                creationDate: row.cells[8].innerHTML,
                requestor: row.cells[9].innerHTML
            }
            selectedSalesOrders.push(data)
        }
    }
}


function fetchSalesOrderApprover() {
    fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/status/allForApprovalStatus`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        } 
        
    }).then(res => res.json()).then(data => {
        if (data.length < 1){
            salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quote Available</h1>`
            salesOrderTable.innerHTML = salesOrder;
        } else {
            salesOrder = data.map(result=> {
                let totalAmountPerSo = 0
                result.items.forEach(item => {
                    let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                    totalAmountPerSo += amountPerLine
                })
                return(
                    `
                    <tr>
                        <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                        <td style="text-align: center;">${result.sapSoId}</td>
                        <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                        <td style="text-align: center;">${result.accountId}</td>
                        <td style="text-align: center;">${result.accountName}</td>
                        <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                        <td style="text-align: center;">${result.externalReference}</td>
                        <td style="text-align: center;">${result.docStatus}</td>
                        <td style="text-align: center;">${result.creationDate}</td>
                        <td style="text-align: center;">${result.requestor}</td>
                    </tr>
                    `
                )
            }).join("")
            salesOrderTable.innerHTML = salesOrder;                       
        }
    })
}

fetchSalesOrderApprover()

// Change of Filter by Status
function statusFilterCallback() {
    let statusFilterValue = document.getElementById('filterStatus').value

    if(statusFilterValue === 'allStatusApprover') {
        fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/status/allStatusApprover`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("")
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'forApprovalStatusApprover') {
        fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/status/allForApprovalStatus`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("")
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'rejectedStatusApprover') {
        fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/status/allRejected`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Rejected Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("")
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } 
}

document.getElementById('filterStatus').onchange = function() {
	statusFilterCallback()
};




// //Prev - Next Page max of 10
// let current_page = 1;
// let records_per_page = 5;;

// let btn_next = document.getElementById("btn_next");
// let btn_prev = document.getElementById("btn_prev");
// // let listing_table = document.querySelector(".salesOrderTable");
// let page_span = document.getElementById("page");
// // let salesOrder;
// // Can be obtained from another source, such as your objJson variable

// function prevPage()
// {
//     if (current_page > 1) {
//         current_page--;
//         changePage(current_page);
//     }
// }

// function nextPage()
// {
//     if (current_page < numPages()) {
//         current_page++;
//         changePage(current_page);
//     }
// }
// function changePage(page)
// {
   

//     // Validate page
//     if (page < 1) page = 1;
//     if (page > numPages()) page = numPages();

//     salesOrderTable.innerHTML = "";

//     // console.log(salesOrderTable);

    

//     for (let i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
//         salesOrderTable.innerHTML += salesOrderTable.length + "<br>";
//     }
//     page_span.innerHTML = page;

//     if (page == 1) {
//         btn_prev.style.visibility = "hidden";
//     } else {
//         btn_prev.style.visibility = "visible";
//     }

//     if (page == numPages()) {
//         btn_next.style.visibility = "hidden";
//     } else {
//         btn_next.style.visibility = "visible";
//     }
// }

// function numPages()
// {
 
//     return Math.ceil(salesOrderTable.length / records_per_page);
    
// }

// window.onload = function() {
//     changePage(1);
// };

