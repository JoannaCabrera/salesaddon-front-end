// Retrieving Sales Order No to View
let params = new URLSearchParams(window.location.search)
let docId = params.get('documentId')

// Global Variables
let token = localStorage.getItem("token")
let accountNameId = ''
let shipToId = ''
let termsId = ''
let taxIdentifier = '';
let productArray = []

let salesOrderNoContainer = document.getElementById('salesOrderId')
let accountNameContainer = document.getElementById('accountName')
let shipToContainer = document.getElementById("shipTo")
let paymentTermsContainer = document.getElementById("paymentTerms");
let creationDateContainer = document.getElementById('createdOn');
let currencyContainer = document.getElementById("currency");
let employeeNameContainer = document.getElementById('requestor')
let employeeIdContainer = document.getElementById('employeeId')
let salesGroupContainer = document.getElementById('salesGroup');
let mbAccountIdContainer = document.getElementById('mbAccountId');
let underToleranceContainer = document.getElementById('underTolerance');
let overToleranceContainer = document.getElementById('overTolerance');
let orderDeliveryDateContainer = document.getElementById('deliveryDate');
let externalReferenceContainer = document.getElementById('externalReference')
let tpsContainer = document.getElementById('tps');
let tpsValueContainer = document.getElementById('tpsValue')
let docStatusContainer = document.getElementById('status')
let requestedDateContainer = document.getElementById('requestedDate')
let sapSoNumberContainer = document.getElementById('sapSoNumber')
let commentContainer = document.getElementById('comment')
let addressContainer = document.getElementById('address')
let custInformationContainer = document.getElementById('custInformation')
let deliveryBlockContainer = document.getElementById('deliveryBlock')
let isExternalReferenceUnique = false;

let productContainer = document.getElementById('product');
let productDescriptionContainer = document.getElementById("productDescription");
let leadTimeContainer = document.getElementById('leadTime');
let quantityContainer = document.getElementById('quantity')
let uomContainer = document.getElementById("uom");
let listPriceContainer = document.getElementById("listPrice");
let discountContainer = document.getElementById('discount')
let netPriceContainer = document.getElementById('netPrice');
let taxAmountContainer = document.getElementById('taxAmount')
let netValueContainer = document.getElementById('netValue')
let moqContainer = document.getElementById('moq');
let stockContainer = document.getElementById('stocks')
let partNumberContainer = document.getElementById('partNumber')
let leadTime = '';
let productCategory = '';

let totalNetValueContainer = document.getElementById('totalNetValue')
let totalTaxAmountContainer = document.getElementById('totalTaxAmount')
let itemstotalAmountContainer = document.getElementById('itemstotalAmount')


// Sales Order No.
let documentIdContainer = document.getElementById('documentId')
documentIdContainer.innerHTML = docId

// Logout function
function logoutFunction() {
	localStorage.clear();
	window.location.replace('./logout.html')
}

document.getElementById('userIcon').onclick = function() {
	logoutFunction();
}

//Check CSL over In-prep and for-Approval

function updateCSL(){
	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase/${accountNameId}`,{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {

		let totalValueForSpecificCustomer = 0

        for(a = 0; a < result.length; a++) {
            let totalAmountPerItem = 0
            totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
            totalValueForSpecificCustomer += totalAmountPerItem
        }

		let creditLimit = document.getElementById ("creditLimitContainer")
		let csl = localStorage.getItem("csl");
       
		updatedCreditLimit = csl - totalValueForSpecificCustomer	
		creditLimit.value = updatedCreditLimit.toFixed(2)	
		
		console.log(totalValueForSpecificCustomer) 	
		console.log(csl)
		console.log(updatedCreditLimit)
})
}
 
// Populating Fields
let itemContainer = document.getElementById("existingProduct");

fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/${docId}`, {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	accountNameId = data.accountId
	shipToId = data.shipToPartyId
	termsId = data.paymentTermsId
	addressContainer.value = data.address
	accountNameContainer.value = data.accountName
	docStatusContainer.value = data.docStatus
	shipToContainer.value = data.shipToPartyDescription
	salesOrderNoContainer.value = data.salesOrderNo
	paymentTermsContainer.value = data.paymentTerms
	creationDateContainer.value = data.creationDate
	externalReferenceContainer.value = data.externalReference
	currencyContainer.value = data.currency
	requestedDateContainer.value = data.requestedDate
	sapSoNumberContainer.value = data.sapSoId
	commentContainer.value = data.comments
	custInformationContainer.value = data.customerInformation
	employeeNameContainer.value = data.requestor
	employeeIdContainer.value = data.employeeId
	salesGroupContainer.value = data.salesGroup
	mbAccountIdContainer.value = data.mbAccountId
	underToleranceContainer.value = data.underTolerance
	overToleranceContainer.value = data.overTolerance
	orderDeliveryDateContainer.value = data.deliveryDate
	deliveryBlockContainer.value =data.deliveryBlockReason,
	tpsValueContainer.innerHTML = data.tps

	console.log("test")

	let credit = document.getElementById('creditLimit');
	let url =  'https://vast-shelf-95326.herokuapp.com/getLimit/'
	let query = data.accountId
	let address = url + query
	await =
	fetch(address, {
	}).then( res => res.text())
	.then( data => {
	let trim = data.replace('",{"currencyCode":"PHP"}]', '')
	let csl = trim.replace('["', '')
	csl.value = csl.value
	localStorage.setItem("csl",csl);
	
	})
	

	let productContainer = document.getElementById("productTableContainer")
	let items;	
	items = data.items.map(result => {
		productArray.push(result)
		return (  
			`
			<tr>
				<td style="text-align: center;">${productArray.length}</td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.productId}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.productDescription}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.leadTime}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.partNumber}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.quantity}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.stocks}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.moq}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.unitOfMeasure}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.listPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.discount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.netPrice}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.taxAmount}" disabled></td>
				<td><input type="text" style="width: 100%; text-align: center;" value="${result.netValue}" disabled></td>
				<td>
					<div class="d-flex justify-content-center">
						<button class="crudButton button btn ms-1 removeButton" id="removeButton" onclick="remove_tr(this)" disabled>
							<i class="far fa-trash-alt"></i>
						</button>
					</div>
				</td>
			</tr>
			`
		)
	}).join("")
	productContainer.innerHTML = items;
}).then(() => {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/customer/view/specific/${accountNameContainer.value}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {
		taxIdentifier = result.taxStatus
		updateTotalNetValue();
		updateTotalTaxAmount();
		calculateTotalAmount();
		updateCSL();
	})
	
});

// Remove Line Item Function
function remove_tr(This) {
	productArray.splice(This.closest('tr').rowIndex - 1, 1);
	This.closest('tr').remove();
}

// Buttons
let buttonContainer = document.getElementById("buttonRow")
let docStatus = document.getElementById('status')

if(localStorage.getItem("isApprover") === "true") {
	buttonContainer.innerHTML = 
	`
	<button id="approveButton">Approve</button>
	<button id="rejectButton">Reject</button>
	<button id="closeButton">Close</button>
 	`

	document.getElementById("approveButton").onclick = function(e) {
		e.preventDefault()
		if(docStatus.value === 'Approved') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else if(docStatus.value === 'Rejected') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else {
			// approveFunction()
			console.log('Approve Function')
		}
	}

	document.getElementById("rejectButton").onclick = function(e) {
		e.preventDefault()
		if(docStatus.value === 'Approved') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else if(docStatus.value === 'Rejected') {
			alert(`Sales Quote is already ${docStatus.value}`)
		} else {
			rejectFunction()
		}
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault()
		closeFunctionApprover()
	}
};

// Approve Function
function approveFunction() {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/approve/${docId}`, {
		method: 'POST',
	}).then(() => {
		alert('Successfully Posted the Sales Quote')
		window.location.replace('./salesOrderMonitoringFA.html');
	})
}

// Reject Function
function rejectFunction() {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/reject/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(result => {
		alert('Successfully Rejected the Sales Quote')
		window.location.reload()
	})
}

// Close Function
function closeFunctionApprover(){
	window.location.replace('./salesOrderMonitoringFA.html')
}

let buttonContainerView = document.getElementById("buttonRow")

if(localStorage.getItem("isRequestor") === "true") {

	buttonContainerView.innerHTML =
	`
	<button id="newButton">New</button>
	<button id="saveButton">Save</button>
	<button id="editButton">Edit</button>
	<button id="postButton">Post</button>	
	<button id="closeButton">Close</button>
	`

	document.getElementById('newButton').onclick = function(e) {
		e.preventDefault();
		newFunction();
	}

	document.getElementById('saveButton').onclick = function(e) {
		e.preventDefault();
		saveFunction();
	};

	document.getElementById('editButton').onclick = function(e) {
		e.preventDefault();
		if(docStatusContainer.value === 'In Preparation') {
			editFunction()
		} else if(docStatusContainer.value === 'Returned') {
			editFunction()
		} else {
			alert(`Error! Sales Quote is already ${docStatusContainer.value}`)
		}
	};

	document.getElementById('postButton').onclick = function(e){
		e.preventDefault()
		if(deliveryBlockContainer.value ==='') {
			postFunction()
		} else {
			let confirm =	window.confirm(`Error! Sales Quote has credit limit block. Please submit for approval.`)
			submitForApprovalFunction() 
		}
	}

	document.getElementById("closeButton").onclick = function(e) {
		e.preventDefault();
		closeFunction();
	}
}
	
// New Function
function newFunction() {
	window.location.replace('./salesOrderCreation.html')
}

// Save Function
function saveFunction() {
	accountNameContainer.setAttribute('readonly', true)
	shipToContainer.setAttribute('readonly', true)
	paymentTermsContainer.setAttribute('readonly', true)
	externalReferenceContainer.setAttribute('readonly', true)
	requestedDateContainer.setAttribute('readonly', true)
	commentContainer.setAttribute('readonly', true)
	custInformationContainer.setAttribute('readonly', true)
	currencyContainer.setAttribute('readonly', true)
	productContainer.setAttribute('readonly', true)
	tpsContainer.setAttribute('readonly', true)
	listPriceContainer.setAttribute('readonly', true)
	partNumberContainer.setAttribute('readonly', true)
	document.getElementById("addItems").setAttribute('disabled', true);

	// Setting disabled attribute into old product array
	let oldItems = document.getElementById("productTableContainer");
	let oldRemove = oldItems.getElementsByTagName("button");

	for(let i = 0; i < oldRemove.length; i++) {
		oldRemove[i].setAttribute('disabled', true)
	}

	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/edit/${docId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			address: addressContainer.value,
			salesOrderNo: salesOrderNoContainer.value,
			accountId: accountNameId,
			accountName: accountNameContainer.value,
			shipToPartyId: shipToId,
			shipToPartyDescription: shipToContainer.value,
			paymentTermsId: termsId,
			paymentTerms: paymentTermsContainer.value,
			requestedDate: requestedDateContainer.value,
			externalReference: externalReferenceContainer.value,
			comments: commentContainer.value,
			custInformation: custInformationValue,
			docStatus: docStatusContainer.value,
			creationDate: creationDateContainer.value,
			currency: currencyContainer.value,
			requestor: employeeNameContainer.value,
			employeeId: employeeIdContainer.value,
			salesGroup: salesGroupContainer.value,
			mbAccountId: mbAccountIdContainer.value,
			underTolerance: underToleranceContainer.value,
			overTolerance: overToleranceContainer.value,
			deliveryDate: orderDeliveryDateContainer.value,
			tps: tpsContainer.value,
			deliveryBlockReason: deliveryBlockContainer.value,
			sapSoId: sapSoNumberContainer.value,
			items: productArray
		})
	}).then(() => {
		alert(`Successfully Edited the Sales Quote ${docId}!`)
		window.location.reload()
	})
}

// Edit Function
function editFunction() {
	accountNameContainer.removeAttribute('readonly')
	shipToContainer.removeAttribute('readonly')
	paymentTermsContainer.removeAttribute('readonly')
	externalReferenceContainer.removeAttribute('readonly')
	requestedDateContainer.removeAttribute('readonly')
	commentContainer.removeAttribute('readonly')
	custInformationContainer.removeAttribute('readonly')
	currencyContainer.removeAttribute('readonly')
	productContainer.removeAttribute('readonly')
	tpsContainer.removeAttribute('readonly')
	document.getElementById("addItems").removeAttribute('disabled')
	document.getElementById("removeButton").removeAttribute('disabled')

	// Removing disabled attribute into old product array
	let oldItems = document.getElementById("productTableContainer");
	let oldRemove = oldItems.getElementsByTagName("button");

	for(let i = 0; i < oldRemove.length; i++) {
		oldRemove[i].removeAttribute('disabled')
	}

	// Change Status to In Preparation
	let inStatus = document.getElementById("status")
	inStatus.value = 'In Preparation'

	//Look Up - Account Name
	let accountsDatalist = document.getElementById("accounts")
	let account;

	fetch('https://fierce-crag-19923.herokuapp.com/api/customer/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		account = data.map(result => {
			return  (
				account =
				`
				<option value="${result.accountName}">
				${result.accountId} - ${result.searchName}
				</option>
				`
			)
		})
		accountsDatalist.innerHTML = account;
	}) 
	
//Check CSL over In-prep and for-Approval


function updateCSL(){
	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/allSalesOrderinDatabase/${accountNameId}`,{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(result => {

		let totalValueForSpecificCustomer = 0

        for(a = 0; a < result.length; a++) {
            let totalAmountPerItem = 0
            totalAmountPerItem = parseFloat(result[a].taxAmount) + parseFloat(result[a].netValue)
            totalValueForSpecificCustomer += totalAmountPerItem
        }

		let creditLimit = document.getElementById ("creditLimit")
		let csl = localStorage.getItem("csl");
       
		updatedCreditLimit = csl - totalValueForSpecificCustomer	
		creditLimit.value = updatedCreditLimit.toFixed(2)	
		
		console.log(totalValueForSpecificCustomer) 		
		console.log(csl)
		console.log(updatedCreditLimit)
})
}

	//Populate Fields - Ship To Location, Payment Terms, Currency, Tax Identifier, Address
	async function populateCustomerFields() {
		fetch(`https://fierce-crag-19923.herokuapp.com/api/customer/view/specific/${accountNameContainer.value}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			addressContainer.value = data.address
			salesGroupContainer.value = data.salesGroup
			mbAccountIdContainer.value = data.mbAccountId
			underToleranceContainer.value = data.underTolerance
			overToleranceContainer.value = data.overTolerance
			accountNameId = data.accountId
			shipToId = data.shipToPartyId
			shipToContainer.value = data.shipToPartyDescription
			termsId = data.paymentTermsId
			paymentTermsContainer.value = data.paymentTerms
			currencyContainer.value = data.currency
			taxIdentifier = data.taxStatus
			}).then(() => {
				fetchBydCsl()
			})
		}
		
	function fetchBydCsl() {
		let credit = document.getElementById('creditLimit');
		let url =  'https://vast-shelf-95326.herokuapp.com/getLimit/'
		let query = accountNameId
		let address = url + query
	
		fetch(address, {
			method: 'GET'
		}).then(res => res.text()).then(data => {
			let trim = data.replace('",{"currencyCode":"PHP"}]', '')
			let csl = trim.replace('["', '')
			credit.value = csl.value
			localStorage.setItem("csl",csl);
		}).then(() => {
			updateCSL()
		})
	}

	//Look Up - Currency
	function currencyLookUp() {
		let currencyDatalist = document.getElementById('currencies')
		let curr;
		fetch('https://fierce-crag-19923.herokuapp.com/api/currency/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			curr = data.map(result => {
				return  (
					curr =
					`
					<option value="${result.currencyTicker.toUpperCase()}">
					${result.currencyName}
					</option>
					`
				)
			})
			currencyDatalist.innerHTML = curr;
		}) 
	}

	// Look Up - Payment Terms
	function paymentTermsLookUp() {
		let termsDatalist = document.getElementById('terms')
		let terms;

		fetch('https://fierce-crag-19923.herokuapp.com/api/terms/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			terms = data.map(result => {
				return  (
					`
					<option value="${result.paymentTermsDescription}">
					${result.paymentTermsId}
					</option>
					`
				)
			})
			termsDatalist.innerHTML = terms;
		}) 
	}

	// Look Up - Ship To Location Customer
	function shipToLocationCustomerLookUp() {
		let locationDatalist = document.getElementById('locations')
		let location;

		fetch('https://fierce-crag-19923.herokuapp.com/api/shipToLocationCustomer/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			location = data.map(result => {
				return  (
					`
					<option value="${result.shipToPartyDescription}">
					${result.shipToPartyId}
					</option>
					`
				)
			})
			locationDatalist.innerHTML = location;
		}) 
	}

	accountNameContainer.oninput = function() {
		populateCustomerFields()
		currencyLookUp()
		paymentTermsLookUp()
		shipToLocationCustomerLookUp()
	};

	// Generate Order Delivery Date
	function generateOrderDeliveryDate() {
		let deliveryDate = document.getElementById('deliveryDate')
		let today = new Date()
		let newDate = new Date()
		newDate.setDate(today.getDate() + parseInt(leadTime))

		newMonth = newDate.toLocaleString('default', {month: 'long'})

		if(newMonth === 'January'){
			newMonth = '01'
		} else if(newMonth === 'February') {
			newMonth = '02'
		} else if(newMonth === 'March') {
			newMonth = '03'
		} else if(newMonth === 'April') {
			newMonth = '04'
		} else if(newMonth === 'May') {
			newMonth = '05'
		} else if(newMonth === 'June') {
			newMonth = '06'
		} else if(newMonth === 'July') {
			newMonth = '07'
		} else if(newMonth === 'August') {
			newMonth = '08'
		} else if(newMonth === 'September') {
			newMonth = '09'
		} else if(newMonth === 'October') {
			newMonth = '10'
		} else if(newMonth === 'November') {
			newMonth = '11'
		} else if(newMonth === 'December') {
			newMonth = '12'
		}

		newDay = newDate.getDate()
		newYear = newDate.getFullYear()
		
		deliveryDate.value = `${newMonth}/${newDay}/${newYear}`
	}

	// Look Up - TPS
	let tpsList = document.getElementById("tps")
	let tps;

	fetch('https://fierce-crag-19923.herokuapp.com/api/tps/view/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		tps = data.map(result => {
			return  (
				`
				<option value="${result.code}">${result.code} - ${result.vendorAccount}</option>
				`
			)
		})
		tpsList.innerHTML = tps;      
	})

	//Look Up - Products
	let productDatalist = document.getElementById('products')
	let product;
	fetch('https://fierce-crag-19923.herokuapp.com/api/products/view/allProducts', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json()).then(data => {
		product = data.map(result => {
			return  (
				`
				<option value="${result.materialId}">
				${result.materialDescription}
				</option>
				`
			)
		})
		productDatalist.innerHTML = product;
	})

	//Populate Fields - Product Description , Unit of Measure, List Price, Net Price, Lead Time
	function populateProductFields() {
		fetch(`https://fierce-crag-19923.herokuapp.com/api/products/view/specific/${productContainer.value}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			productDescriptionContainer.value = data.materialDescription
			listPriceContainer.value = data.price
			uomContainer.value = data.uomDescription
			netPriceContainer.value = data.price
			leadTimeContainer.value = data.leadTime
			productCategory = data.productCategoryId
			stockContainer.value = data.stock
			partNumberContainer.value = data.partNumber

			if(leadTime === '') {
				leadTime = data.leadTime
			} else if(leadTime < parseInt(data.leadTime)) {
				leadTime = data.leadTime
			}
		}).then(() => {
			validateMoq()
		})
	}

	function enableFields() {
		uomContainer.removeAttribute('readonly')
		quantityContainer.removeAttribute('readonly')
		discountContainer.removeAttribute('readonly')
		listPriceContainer.removeAttribute('readonly')
		partNumberContainer.removeAttribute('readonly')
	}

	//Look Up - Unit of Measure
	function uomLookUp() {
		let uomDatalist = document.getElementById('uoms')
		let uom;
		fetch('https://fierce-crag-19923.herokuapp.com/api/uom/view/all', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			uom = data.map(result => {
				return  (
					uom =
					`
					<option value="${result.description}">
					${result.code}
					</option>
					`
				)
			})
			uomDatalist.innerHTML = uom;
		})
	}

	productContainer.oninput = function() {
		populateProductFields()
		enableFields()
		uomLookUp()
	};

	// Look-Up and Validation of Minimum Order Quantity
	function validateMoq() {
		fetch(`https://fierce-crag-19923.herokuapp.com/api/moq/validate/${productCategory}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(result => {
			if(result === true) {
				// 8754M-050B-00BLK
				fetch(`https://fierce-crag-19923.herokuapp.com/api/moq/view/specific/${productCategory}`, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(res => res.json()).then(data => {
					moqContainer.value = data.minimumOrderQuantity
				})
			} else {
				moqContainer.value = 0
			}
		})
	}

	// Generating Net Value
	function generateNetValue() {
		let quantityValue = document.getElementById('quantity').value
		let netValueContainer = document.getElementById('netValue')
		let listPriceValue = document.getElementById('listPrice').value

		netValueContainer.value = quantityValue * listPriceValue
	};

	quantityContainer.oninput = function() {
		generateNetValue()
	};

	// Changing the Net Price based on Discount Field
	function applyDiscount(discountValue) {
		let netPriceValue = document.getElementById("netPrice").value
		let listPriceValue = document.getElementById("listPrice").value
		netPriceValue = listPriceValue;
		discountValue = discountValue/100
		netPriceValue = (netPriceValue * (1 - discountValue)).toFixed(2)

		changeNetPrice(netPriceValue)
	}

	function changeNetPrice(netPriceValue) {
		let netPriceContainer = document.getElementById("netPrice")
		let netValueContainer = document.getElementById("netValue")
		let quantityValue = document.getElementById("quantity").value
	
		netPriceContainer.value = netPriceValue
		netValueContainer.value = (quantityValue * netPriceContainer.value).toFixed(2)
	
		calculateTaxAmount();
	}

	function calculateTaxAmount() {
		let netValueValue = document.getElementById("netValue").value
		let taxAmountContainer = document.getElementById("taxAmount")
	
		if(taxIdentifier === 'Vatable') {
			taxAmountContainer.value = (netValueValue * 0.12).toFixed(2)
		} else {
			taxAmountContainer.value = netValueValue * 0
		}
		
		itemsPush();
		itemsDisplay();
		clearFunction();
	}

	//Array for Products/Items
	function itemsPush() {	
		productArray.push({
			productId: productContainer.value,
			productDescription: productDescriptionContainer.value,
			leadTime: leadTimeContainer.value,
			partNumber: partNumberContainer.value,
			quantity: quantityContainer.value,
			stocks: stockContainer.value,
			moq: moqContainer.value,
			unitOfMeasure: uomContainer.value,
			listPrice: listPriceContainer.value,
			discount: discountContainer.value,
			netPrice: netPriceContainer.value,
			taxAmount: taxAmountContainer.value,
			netValue: netValueContainer.value,
		})
	}

	// Add Row
	let productTableContainer = document.getElementById("productTableContainer");
	let items;

	function itemsDisplay(){
		let a = 0;
		items = productArray.map(itemsList=> {
			return(
				a++,
				`
				<tr>
					<td>${a}</td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productId}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.productDescription}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.leadTime}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.partNumber}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.quantity}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.stocks}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.moq}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.unitOfMeasure}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.listPrice}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.discount}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netPrice}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.taxAmount}" disabled></td>
					<td><input type="text" style="width: 100%; text-align: center;" value="${itemsList.netValue}" disabled></td>
					<td>
					<div class="d-flex justify-content-center">
						<button class="crudButton button btn ms-1" type="button" onclick="remove_tr(this)">
							<i class="far fa-trash-alt"></i>
						</button>
					</div>
					
					</td>
			
				</tr>
				
				`
			)
		}).join("");
		productTableContainer.innerHTML = items; 
	}

	// To Clear Look Ups
	function clearFunction() {
		productContainer.value = ''
		productDescriptionContainer.value = ''
		leadTimeContainer.value = ''
		partNumberContainer.value = ''
		quantityContainer.value = ''
		stockContainer.value = ''
		moqContainer.value = ''
		uomContainer.value = ''
		listPriceContainer.value = ''
		discountContainer.value = ''
		netPriceContainer.value = ''
		taxAmountContainer.value = ''
		netValueContainer.value = ''
	}

	function disableFields() {
		uomContainer.setAttribute('readonly', true)
		quantityContainer.setAttribute('readonly', true)
		discountContainer.setAttribute('readonly', true)
	}

	document.getElementById("addItems").onclick = function(e) {
		e.preventDefault();
		if(quantityContainer.value < moqContainer.value) {
			let confirm = window.confirm ("Ordered Quantity is less than Minimum Order Quantity. Do you want to proceed?");
			if (confirm === true){
				let discountValue = discountContainer.value
				applyDiscount(discountValue);
				disableFields();
				generateOrderDeliveryDate();
				updateTotalNetValue();
				updateTotalTaxAmount();
				calculateTotalAmount();
			}
		} else {
			let discountValue = discountContainer.value
			applyDiscount(discountValue);
			disableFields();
			generateOrderDeliveryDate();
			updateTotalNetValue();
			updateTotalTaxAmount();
			calculateTotalAmount();
		}

		function updateTotalNetValue() {
			console.log('updateTotalNetValue')
			let sumOfAllNetValue = 0
			productArray.forEach(item => {
				sumOfAllNetValue += parseFloat(item.netValue)
			})
			totalNetValueContainer.value = sumOfAllNetValue.toFixed(2)
		}
		
		function updateTotalTaxAmount() {
			console.log('updateTotalTaxAmount')
			let sumOfAllTaxAmount = 0
			productArray.forEach(item => {
				sumOfAllTaxAmount += parseFloat(item.taxAmount)
			})
			totalTaxAmountContainer.value = sumOfAllTaxAmount.toFixed(2)
		}
		
		function calculateTotalAmount() {
			console.log('calculateTotalAmount')
			let sumOfAllItemsTotalAmount = 0
			productArray.forEach(item => {
				sumOfAllItemsTotalAmount += parseFloat(item.taxAmount) + parseFloat(item.netValue)
			})
			itemstotalAmountContainer.value = sumOfAllItemsTotalAmount.toFixed(2)
		}
	};

	// Remove Functions
	function remove_tr(This) {
		itemsArray.splice(This.closest('tr').rowIndex - 1, 1);
		This.closest('tr').remove();

		updateTotalNetValue();
		updateTotalTaxAmount();
		calculateTotalAmount();
	}
}

function updateTotalNetValue() {
	let sumOfAllNetValue = 0
	productArray.forEach(item => {
		sumOfAllNetValue += parseFloat(item.netValue)
	})
	totalNetValueContainer.value = sumOfAllNetValue.toFixed(2)
}

function updateTotalTaxAmount() {
	let sumOfAllTaxAmount = 0
	productArray.forEach(item => {
		sumOfAllTaxAmount += parseFloat(item.taxAmount)
	})
	totalTaxAmountContainer.value = sumOfAllTaxAmount.toFixed(2)
}

function calculateTotalAmount() {
	let sumOfAllItemsTotalAmount = 0
	productArray.forEach(item => {
		sumOfAllItemsTotalAmount += parseFloat(item.taxAmount) + parseFloat(item.netValue)
	})
	itemstotalAmountContainer.value = sumOfAllItemsTotalAmount.toFixed(2)
}

// Post Function
function postFunction() {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/approve/${docId}`, {
		method: 'POST',
	}).then(() => {
		alert('Successfully Posted the Sales Quote')
		if(localStorage.getItem("isApprove") == "true") {
			window.location.replace('./salesOrderMonitoringFA.html');
		} else {
			window.location.replace('./salesOrderMonitoring.html');
		}
	})
}

// Submit For Approval Function
function submitForApprovalFunction()  {
	fetch(`https://fierce-crag-19923.herokuapp.com/api/salesOrder/salesOrderMonitoring/submitForApproval/${docId}`, {
		method: 'POST',
	}).then(() => {
		alert('Successfully Submitted the Sales Order for Approval')
		if(localStorage.getItem("isApprove") == "true") {
			window.location.replace('./salesOrderMonitoringFA.html');
		} else {
			window.location.replace('./salesOrderMonitoring.html');
		}
	})
}

// Submit For Approval Function
// function submitForApprovalFunction() {
// 	let accountNameValue = accountNameContainer.value
// 	let shipToValue = shipToContainer.value
// 	let addressValue = addressContainer.value
// 	let paymentTermsValue = paymentTermsContainer.value
// 	let requestedDateValue = document.getElementById('requestedDate').value
// 	let externalReferenceValue = externalReferenceContainer.value
// 	let commentsValue = document.getElementById('comment').value
// 	let custInformationValue = document.getElementById('custInformation').value
// 	let creationDateValue = creationDateContainer.value
// 	let currencyValue = currencyContainer.value
// 	let requestorValue = employeeNameContainer.value
// 	let employeeIdValue = employeeIdContainer.value
// 	let salesOrderNoValue = salesOrderNoContainer.value
// 	let salesGroupValue = salesGroupContainer.value
// 	let mbAccountIdValue = mbAccountIdContainer.value
// 	let underToleranceValue = underToleranceContainer.value
// 	let overToleranceValue = overToleranceContainer.value
// 	// let orderDeliveryDateValue = orderDeliveryDate.value
// 	let tpsValue = document.getElementById('tps').value
// 	let deliveryBlockValue = document.getElementById ("deliveryBlock").value


// 	fetch('https://fierce-crag-19923.herokuapp.com/api/salesOrder/add', {
// 		method: 'POST',
// 		headers: {
// 			'Content-Type': 'application/json'
// 		},
// 		body: JSON.stringify({
// 			address: addressValue,
// 			salesOrderNo: salesOrderNoValue,
// 			accountId: accountNameId,
// 			accountName: accountNameValue,
// 			shipToPartyId: shipToId,
// 			shipToPartyDescription: shipToValue,
// 			paymentTermsId: termsId,
// 			paymentTerms: paymentTermsValue,
// 			requestedDate: requestedDateValue,
// 			externalReference: externalReferenceValue, 
// 			comments: commentsValue,
// 			customerInformation: custInformationValue,
// 			docStatus: 'For Approval',
// 			creationDate: creationDateValue,
// 			currency: currencyValue,
// 			requestor: requestorValue,
// 			employeeId: employeeIdValue,
// 			salesGroup: salesGroupValue,
// 			mbAccountId: mbAccountIdValue,
// 			underTolerance: underToleranceValue,
// 			overTolerance: overToleranceValue,
// 			tps: tpsValue,
// 			deliveryBlockReason: deliveryBlockValue,
// 			items: itemsArray
// 		})
// 	}).then(res => {
// 		alert('Successfully Submitted the Sales Order for Approval')
// 		window.location.reload()
// 	})
// }

// Close Function
function closeFunction(){
	window.location.replace('./salesOrderMonitoring.html')	
}